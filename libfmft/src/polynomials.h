/******************************************************************************
Copyright (c) 2015 Dzmitry Padhorny, Andrey Kazennov and Dima Kozakov

Permission is hereby granted, free of charge, to any person obtaining a copy 
of this software and associated documentation files (the "Software"), to deal 
in the Software without restriction, including without limitation the rights 
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
SOFTWARE.
*******************************************************************************/
/** \file polynomials.h
	This file contains functions
	that are used in computing spherical and radial harmonics.
*/
#pragma once
#include <errno.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "index.h"
#define MAXSLEN 200



////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
// The important functions.

/**
 * Standard LM indexer.
 * \param N Highest polynomial order.
 * \param l In range [0, N-1]
 * \param m In range [-l, l]
 */
inline int index_LM(int N, int l, int m)
{
	return N*(m + N-1) + l;
}


/**
 * Standard NL indexer.
 * \param N Highest polynomial order.
 * \param n In range [1, N]
 * \param l In range [0, n-1]
 */
inline int index_NL(int N, int n, int l)
{
	return (N * (l) + (n-1));
}

/**
 * Standard indexer for Laguerre arrays.
 * \param N Highest polynomial order.
 */
inline int index_Laguerre(int N, int lower, int upper)
{
	return (lower*N - lower*(lower - 1)/2 + upper);
}

/**
	Generates an N*N array of
	radial harmonics' normalization coefficients
	for all radial harmonics up to order N.
	\param N Highest polynomial order of radial harmonics.
	\param k Scaling parameter (Set it to 20.0 if you don't know what it is).
	\return Pointer to the generated array. Individual elements of this array can be accessed with index_NL() indexer.
 */
double* generate_radial_norm_HarmOsc(int N, float k);


/**
	Fills a pre-allocated array R
	with radial harmonics' values
	calculated for specific parameter value r.
	\param R Pointer to a pre-allocated array of size (N*N). Individual elements of this array can be acessed with index_NL() indexer.
	\param R_norm Pointer to an array of normalization values calculated by the function generate_radial_norm_Ritchie().
	\param N Highest polynomial order of spherical harmonics.
	\param r Distance from the center of coordinate system.
	\param k Scaling parameter (Set it to 20.0 if you don't know what it is).
 */
void fill_radial_harmonics_array_HarmOsc(double* R, double* R_norm, int N, float r, float k);


/**
	Generates an N*N normalization
	array for a set of spherical harmonics
	up to order N. (Y(l,m) = norm_l,m * P_lm(cos(theta)) * exp(i*m*phi))
	\param N Highest polynomial order of spherical harmonics.
	\return Pointer to the generated array. Individual elements of this array can be accessed with index_LM() indexer.
 */
double* generate_spherical_norm(int N);


/**
	Fills a pre-allocated array P
	with associated Legendre polynomials' values
	calculated for specific parameter value x.
	\param N Highest polynomial order of spherical harmonics.
	\param x Associated Legendre polinomials' parameter.
	\param P Pointer to a pre-allocated array of size (N*(2*N - 1)). Individual elements of this array can be accessed with index_LM() indexer.
 */
void fill_assoc_Legendre_array(int N, double x, double* P);

/**
 * Generates an N*N array of Laguree
 * polynomials calculated for parameter x.
 * \param  N Highest order for which the polynomials will be computed.
 * \param x Laguerre polynomials' parameter.
 * \param alpha_min ???.
 * \return Pointer to the generated array. Individual elements of this array can be accessed with index_Laguerre() indexer.
 */
double* generate_laguerre_array_for_HarmOsc(int N, float x, float alpha_min);

////////////////////////////////////////////////////////////////////
// Some utilites That might not be needed, but are used at present.


///////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// Should get rid of this

