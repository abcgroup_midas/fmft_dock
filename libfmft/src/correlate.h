/******************************************************************************
Copyright (c) 2015 Dzmitry Padhorny, Andrey Kazennov and Dima Kozakov

Permission is hereby granted, free of charge, to any person obtaining a copy 
of this software and associated documentation files (the "Software"), to deal 
in the Software without restriction, including without limitation the rights 
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
SOFTWARE.
*******************************************************************************/
#pragma once
#include <stdio.h>
#include <float.h>
#include <algorithm>
#include <vector>
#include "index.h"
#include <fftw3.h>
#include <math.h>
//#include <time.h>
#include <stdlib.h>
#include <string.h>
#include "translational_matrix.h"
#include "rotational_matrix.h"
#include "spf_coefficients.h"
#include "angle_distance.h"
#include "common.h"


////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
// The important functions.
struct Score
{
    double val;
    int beta;
    int gamma;
    int alpha1;
    int beta1;
    int gamma1;
    double Z;
};

struct fft_data
{
	int FFT_size;
	__fftw_complex* W;
	__real* E;
	__fftw_plan plan;
};

inline int index_loop2(int S, int s, int t, int m, int j, int l)
{
	//return (N*(N*(N*((2*N-1)*(N - 1 + s) + (N - 1 + t)) + m) + j) + l);
	int HS=S/2;
	return ((HS+1)*((HS+1)*((HS+1)*((S)*(HS + s) + (HS + t)) + m) + j) + l);
}

inline int index_loop3(int S, int beta1, int s, int t, int m, int j)
{
	//return (N*(N*((2*N - 1)*((2*N - 1)*beta1 + (N - 1 + s)) + (N - 1 + t)) + m) + j);
	int HS=S/2;
	return ((HS+1/*?*/)*((HS+1)*((S)*((S)*beta1 + (HS + s)) + (HS + t)) + m) + j);
}

/*
 * Index for the last stage of fft array filling routine.
 * The array is supposed to be used with complex-to-real DFT
 */
inline int index_loop4(int S, int b, int b1, int i, int j, int k)
{
	int HS=S/2;
	return (HS+1)*((S)*((S)*((HS+1)*b + b1) + i) + j) + k;
}

fftw_plan plan_3d_c2r_dft(fftw_complex* W, double *E, const int N);

struct fft_data* prepare_fft_arrays(const int FFT_size);

void fill_fft_array_step1_v1(const struct many_spf_coeffs* mcoeffs, struct fft_data* FFT_arrays, const double *T);

void fill_fft_array_step2(struct fft_data* FFT_arrays, const double *d_small, const int N);

/**
 * This function executes the main part of the docking routine.
 * It takes a set of SPF coefficients calculated for a range of grid pairs
 * and uses them to calculate cross-correlations of the said grid pairs.
 * Cross-correlations are calculated in two steps:
 * 1) At first, a N*N 3D arrays are filled.
 * 2) Then a 3D Fourier transform is applied to these N*N arrays.
 * After that the resulting array undergoes clustering, and
 * score_count conformations are chosen each being the best-scoring in a cluster.
 * The above actions are repeated for each translation step,
 * which eventually gives us ((Z_stop-Z_start) / Z_step + 1) * score_count conformations.
 * These best-scoring conformations are written into best_scores array.
 * \param mcoeffs Pointer to an instance of many_spf_coeffs struct storing SPF coefficients for a range of grid pairs.
 * \param best_scores Pointer to an array of Score structs, where the function writes best-scoring conformations.
 * \param Z_start Minimum translation distance.
 * \param Z_stop Maximim translation distance.
 * \param score_count number of cluster considered for each translation distance.
 * \param max_delta Clustering parameter (angle in radians)
 * \param folder_name folder, in which the executable is residing.
 * (Needed to find the tabulated translation matrix files. These are stored at the folder_name/data directory)
 * \param mpi_nprocs Number of MPI threads. Only relevant when MPI is used.
 * \param mpi_rank   MPI rank. Only relevant when MPI is used.
 */
void score_and_cluster_v1(const struct many_spf_coeffs* mcoeffs,
		                  struct Score *best_scores,
		                  const int FFT_size,
		                  const double Z_start,
		                  const double Z_stop,
		                  const double Z_step,
		                  const int score_count,
		                  const double max_delta,
		                  const char* folder_name,
		                  const int mpi_nprocs,
		                  const int mpi_rank);
void save_score(struct Score *best_scores, int output_score_cout, int N, char* path);

/**
 * This function saves the docking results in the PIPER-like format.
 * \param best_scores Pointer to an array of Score structs containing best-scoring conformations.
 * \param output_score_count The number of conformations frm best_scores array to be written.
 * \param FFT_size FFT size that was used for calculation.
 * \param ref_lig_pose The reference position of ligand input structure relative to receptor.
 * \param output_fname Name of the output ft file to be created.
 * \param rm_fname Name of the output rm file to be created.
 */
void save_score_piper(const struct Score *best_scores,\
		                 const int output_score_count,\
		                 const int FFT_size,\
		                 const struct vector3 ref_lig_pos,\
		                 const char* output_fname,\
		                 const char* rm_fname);
////////////////////////////////////////////////////////////////////
// Some utilites That might not be needed, but are used at present.
/*
 * Index for the scoring array.
 */
inline int index_score(int S, int beta_id, int beta1_id, int gamma_id, int gamma1_id, int alpha1_id)
{
	int HS = S / 2;
	return S*(S*(S*((HS+1)*beta_id + beta1_id) + gamma_id) + gamma1_id) + alpha1_id;
}

struct score_element
{
    double val;
    char beta_id;
    char gamma_id;
    char alpha1_id;
    char beta1_id;
    char gamma1_id;
    double Z;
};

struct angles
{
	int alpha_id;
	int beta_id;
	int gamma_id;
};

struct neighbors
{
	int num;
	int *alpha_id;
	int *beta_id;
	int *gamma_id;
	int howmany;
};


///////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// Should get rid of this

struct trig_tab
{
	int num;
	double *cos;
	double *sin;
	double *beta_cos;
	double *beta_sin;
};

class mycompare_class
{
	public:
	__real* arr;
	bool operator()(int i, int j)
	{
		return (arr[i] < arr[j]);
	}
};

class mycompare_class2
{
	public:
	bool operator()(const struct Score &i, const struct Score &j)
	{
		return (i.val < j.val);
	}
};


inline void swap(double *array, int i, int j)
{
	double temp = array[j];
	array[j] = array[i];
	array[i] = temp;
}

///////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// proper thing
void deallocate_neighbors(struct neighbors *nbs);

void get_rec_neighbors_s2(struct neighbors* nbs, double min_passv_rm[3][3], const int FFT_size, const double theta, const struct trig_tab *tab);

void get_lig_neighbors_so3(struct neighbors* nbs, double min_rm1[3][3], const int N, const double theta, const struct trig_tab *tab);

/**
 * Consequently find minima in the FFT_ARRAYS->E array and clear the surrounding region
 * (i.e exclude all the scores that are closer then theta on S2 and SO(3) to the given minima from further consideration)
 * \param FFT_arrays pointer to the instance of fft_data struct, containing the E array.
 *        E arrays contains the scores for all the orientations of receptor and ligand
 */
void filter_scores(struct fft_data* FFT_arrays, struct Score *best_scores, const int score_count, const double z, const double theta);
///////////////////

void output_sort(struct Score* scores, int num_best, int num_all);
