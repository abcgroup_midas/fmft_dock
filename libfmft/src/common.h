/******************************************************************************
Copyright (c) 2015 Dzmitry Padhorny, Andrey Kazennov and Dima Kozakov

Permission is hereby granted, free of charge, to any person obtaining a copy 
of this software and associated documentation files (the "Software"), to deal 
in the Software without restriction, including without limitation the rights 
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
SOFTWARE.
*******************************************************************************/
#pragma once
#include <fftw3.h>

#ifdef _SINGLE_PREC_
#define __real                   float
#define __fftw_complex           fftwf_complex
#define __fftw_plan              fftwf_plan
	#define _fftw_plan_many_dft_c2r  fftwf_plan_many_dft_c2r
	#define _fftw_malloc             fftwf_malloc
	#define _fftw_execute            fftwf_execute
	#define _fftw_destroy_plan       fftwf_destroy_plan
	#define _fftw_free               fftwf_free
	#define _fftw_cleanup            fftwf_cleanup

	#define __REAL_MAX               FLT_MAX
#else
#define __real                   double
#define __fftw_complex           fftw_complex
#define __fftw_plan              fftw_plan
	#define _fftw_plan_many_dft_c2r  fftw_plan_many_dft_c2r
	#define _fftw_malloc             fftw_malloc
	#define _fftw_execute            fftw_execute
	#define _fftw_destroy_plan       fftw_destroy_plan
	#define _fftw_free               fftw_free
	#define _fftw_cleanup            fftw_cleanup

	#define __REAL_MAX               DBL_MAX
#endif
