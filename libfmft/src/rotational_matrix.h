/******************************************************************************
Copyright (c) 2015 Dzmitry Padhorny, Andrey Kazennov and Dima Kozakov

Permission is hereby granted, free of charge, to any person obtaining a copy 
of this software and associated documentation files (the "Software"), to deal 
in the Software without restriction, including without limitation the rights 
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
SOFTWARE.
*******************************************************************************/
/** \file rotational_matrix.h
	This file contains structures and functions
	that are used for rotation in SPF space.
*/
#pragma once
#include <string.h>
#include "index.h"
#include "spf_coefficients.h"
#define DFOLDER "data"
#define DFILE "wigner_d"
#define DFILE_NEW "wigner_d_new"

////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
// The important functions.
/**
 * Indexer for
 * arrays storing elements of Wigner d matrix calculated for a single beta angle value.
 * \param L Highest polynomial order, for which the matrix elements are stored.
 * \param l In range [0, L].
 * \param m In range [-l, l].
 * \param m1 In range [-l, l]
 */
inline int index_d_array(int L, int l, int m, int m1)
{
	return (2*L + 1)*((2*L + 1)*l + (L + m)) + (L + m1);
}


/**
 * Indexer for
 * arrays storing elements of Wigner d matrices calculated for a set of different beta angle values.
 * \param L Highest polynomial order, for which the matrices' elements are stored.
 * \param i Index of matrix, calculated for a specific value of Euler beta angle, in range [0, num_angles-1].
 * \param l In range [0, L].
 * \param m In range [-l, l].
 * \param m1 In range [-l, l]
 */
inline int index_many_d_arrays(int L, int i, int l, int m, int m1)
{
	//return (2*L + 1)*((2*L + 1)*((L+1)*b + l) + (L + m)) + (L + m1);
	return (L+1)*((2*L+1)*((2*L + 1)*i + (L + m1)) + (L + m)) + l;
}


/**
 * Indexer for
 * arrays storing elements of Wigner D matrix calculated for Euler angles alpha, beta, gamma.
 * \param L Highest polynomial order, for which the matrix elements are stored.
 * \param l In range [0, L].
 * \param m In range [-l, l].
 * \param m1 In range [-l, l]
 *
 */
inline int index_D_array(int L, int l, int m, int m1)
{
	return (2*L + 1)*((2*L + 1)*l + (L + m)) + (L + m1);
}


/**
 * This struct is used for storing elements of Wigner d matrices,
 * calculated for a single value of Euler beta angle up to polynomial order L.
 */
struct d_array
{
    int L; /**< Highest polynomial order, for which matrix elements are stored. */
    double *data; /**< array of size (L+1)*(2*L + 1)*(2*L + 1). */
};


/**
 * This struct is used for storing elements of Wigner d matrices calculated for a set of different Euler beta angle values up to polynomial order L.
 */
struct many_d_arrays
{
    int L; /**< Highest polynomial order, for which matrix elements are stored. */
    int num_angles; /**< Number of different Euler beta angle values, for which matrix elements are stored*/
    double *data; /**< array of size num_angles*(L+1)*(2*L + 1)*(2*L + 1). */
};


/**
 * This struct is used for storing elements of Wigner D matrix (which serves as rotation matrix in SPF space) up to polynomial order L.
 */
struct D_array
{
	int L; /**< Highest polynomial order, for which matrix elements are stored. */
	double *re; /**< array of size (L+1)*(2*L + 1)*(2*L + 1)*/
	double *im; /**< array of size (L+1)*(2*L + 1)*(2*L + 1)*/
};


/**
 * Allocates an instance of d_array struct
 * that is intended to store Wigner d matrix elements up to polynomial order L.
 * \param L Highest polynomial order, for which matrix elements will be stored.
 * \return Pointer to the allocated struct.
 */
struct d_array* allocate_d_array(const int L);

/**
 * Allocates an instance of many_d_arrays struct
 * that is intended to store elements of num_angles Wigner d matrix up to polynomial order L.
 * \param L Highest polynomial order, for which matrix elements will be stored.
 * \param num_angles Number of different Euler beta angle values, for which matrix elements will be stored.
 * \return Pointer to the allocated struct.
 */
struct many_d_arrays* allocate_many_d_arrays(const int L, const int num_angles);

/**
 * Allocates an instance of D_array struct
 * that is intended to store Wigner D matrix elements up to polynomial order L.
 * \param L Highest polynomial order, for which matrix elements will be stored.
 * \return Pointer to the allocated struct.
 */
struct D_array* allocate_D_array(const int L);


/**
 * Deallocates an instance of d_array struct.
 * \param d Pointer to an instance of d_array struct.
 */
void deallocate_d_array(struct d_array* d);


/**
 * Deallocates an instance of many_d_arrays struct.
 * \param md Pointer to an instance of many_d_arrays struct.
 */
void deallocate_many_d_arrays(struct many_d_arrays* md);


/**
 * Deallocates an instance of D_array struct.
 * \param D Pointer to an instance of D_array struct.
 */
void deallocate_D_array(struct D_array* D);


/**
 * Allocates an instance of d_array struct and
 * fills it with wigner d matrix elements calculated for a
 * single value of Euler beta angle up to polynomial order L.
 * \return Pointer to the filled struct.
 */
struct d_array* generate_d_array(const int L, const double beta);


/**
 * Generates arrays of wigner d_matrix elements
 * calculated for a set of Euler beta angle values and writes
 * them into a file named (DFILE_NEW)_(L).dat in a folder determined
 * by the path parameter.
 * The values of Euler beta angles are taken as i*Pi/(L+1), where i = [0, L].
 * Filename DFILE_NEW is a constant defined in rotational matrix.h.
 * \param L Highest polynomial order, for which matrix elements will be calculated and stored.
 * \param path Path to the folder, in which (DFILE_NEW)_(L).dat file will be created.
 */
void tab_many_d_arrays(const int L, const char *path);


/**
 * Allocates an instance of many_d_arrays struct and
 * fills it according to a file named (DFILE_NEW)_(L).dat located in
 * a folder determined by the path parameter.
 * Filename DFILE_NEW is a constant defined in rotational matrix.h.
 * The file is supposed to contain Wigner d matrix elements calculated
 * for a set of Euler beta angles up to polynomial order L.
 * The values of Euler beta angles are supposed to be i*Pi/(L+1), where i = [0, L].
 * \param L Highest polynomial order, for which matrix elements will be calculated and stored.
 * \param path Path to the folder, in which (DFILE_NEW)_(L).dat file is located.
 * \return Pointer to the filled many_d_arrays struct.
 */
struct many_d_arrays* load_many_d_arrays(const int L, const char *path);

/**
 * This function checks the existence of a file named (DFILE_NEW)_(L).dat
 * in folder determined by the path parameter and, if such file exists,
 * tries to allocate and fill an instance of many_d_arrays struct
 * using load_many_d_arrays() function.
 * If this file is not found, the function creates it using
 * tab_many_d_arrays() function and once again tries to allocate
 * and fill an instance of many_d_arrays struct
 * using load_many_d_arrays() function.
 * \param L Highest polynomial order, for which matrix elements are (will be) calculated and stored.
 * \param path Path to the folder, in which (DFILE_NEW)_(L).dat file is located (or is intended to be created).
 * \return Pointer to the filled many_d_arrays struct.
 */
struct many_d_arrays* get_many_d_arrays(const int L, const char *path);

/**
 * This function allocates and fill an instance of many_d_arrays struct
 * with elements of Wigner d-functions calculated up to polinomial order L
 * for num_angles values of beta angle, covering the range [0, pi] in
 * uniform steps.
 * \param num_angles Number of beta angle values, for which the d-functions will be calculated.
 * \param L Highest polynomial order, for which matrix elements are (will be) calculated and stored.
 * \return Pointer to the filled many_d_arrays struct.
 */
struct many_d_arrays* get_many_d_arrays_new(const int num_angles, const int L);

/**
 * Allocates an instance of D_array struct and
 * fills it with wigner D matrix elements calculated for Euler
 * angles alpha, beta, gamma up to polynomial order L.
 * \param L Highest polynomial order, for which matrix elements will be calculated and stored.
 * \param alpha Alpha Euler angle.
 * \param beta Beta Euler angle.
 * \param gamma Gamma Euler angle.
 * \return Pointer to the filled struct.
 */
struct D_array* generate_D_array(const int L, const double alpha, const double beta, const double gamma);


/**
 * Allocates an instance of spf_coeffs struct and
 * fills it with the result of multiplication of a set of SPF
 * coefficients stored by coeffs spf_coeffs struct by a D wigner D matrix
 * (Which performs rotation of the property represented by the coeffs set of SPF coefficients
 * by Euler angles alpha, beta, gamma, for which the D wigner D matrix was calculated).
 * Note that the highest polynomial order of a set of SPF coefficients N is supposed to be related to the highest
 * polynomial order of Wigner D matrix L as L=N-1.
 * \param coeffs Pointer to an instance of spf_coeffs struct, storing a set of SPF coefficients to be rotated.
 * \param D Pointer to an instance of D_array struct, storing elements of a  Wigner D matrix.
 * \return Pointer to the instance of spf_coeffs struct, filled with "rotated" coefficients.
 */
struct spf_coeffs* rotate_spf_coeffs(const struct spf_coeffs* coeffs, const struct D_array* D);


////////////////////////////////////////////////////////////////////
// Some utilites That might not be needed, but are used at present.
void calc_delta(struct matrix_3d* delta, const int L);

/**
	Calculates the elements of the rotation matrix
	for Euler angles alpha, beta, gamma.
	\param alpha Euler angle
	\param beta Euler angle
	\param gamma Euler angle
	\return Pointer to the matrix (3 x 3 array)
*/
double** create_rotation_matrix (double alpha, double beta, double gamma);


/**
	Calculates the elements of the rotation matrix
	for Euler angles a, b, g for normal rotation.
	\param rm  Rotation matrix to be filled
	\param a Euler angle (radians)
	\param b Euler angle (radians)
	\param g Euler angle (radians)
*/
void fill_active_rotation_matrix (double rm[3][3], double a, double b, double g);

/**
	Calculates the elements of the rotation matrix
	for Euler angles a, b, g for passive rotation
	(this matrix will actually be a transposed normal rotation matrix).
	\param rm  Rotation matrix to be filled
	\param a Euler angle (radians)
	\param b Euler angle (radians)
	\param g Euler angle (radians)
*/
void fill_passive_rotation_matrix (double rm[3][3], double a, double b, double g);


#ifndef DOXYGEN_SHOULD_SKIP_THIS
///////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// Should get rid of this
/*** MATRIX 3D BASIC ***/

struct matrix_3d
{
    int n;
    double *data; // Array of size n^3
};

// (de)allocators
struct matrix_3d* allocate_matrix_3d(int n);
void free_matrix_3d(struct matrix_3d* M);

// basic accessors
size_t index_matrix_3d(const struct matrix_3d* M, int n, int l, int m);
double get_matrix_3d(const struct matrix_3d* M, int n, int l, int m);
void set_matrix_3d(struct matrix_3d* M, int n, int l, int m, const double val);
#endif // DOXYGEN_SHOULD_SKIP_THIS
