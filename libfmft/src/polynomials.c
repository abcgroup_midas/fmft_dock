/******************************************************************************
Copyright (c) 2015 Dzmitry Padhorny, Andrey Kazennov and Dima Kozakov

Permission is hereby granted, free of charge, to any person obtaining a copy 
of this software and associated documentation files (the "Software"), to deal 
in the Software without restriction, including without limitation the rights 
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
SOFTWARE.
*******************************************************************************/
#include "polynomials.h"


////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
// The important functions.
double* generate_radial_norm_HarmOsc(int N, float k)
{
	double* norm = (double*)calloc(N*N, sizeof(double));

	norm[index_NL(N, 1, 0)] = sqrt(2.00);
	for (int n = 2; n <= N; ++n)
	{
		norm[index_NL(N, n, 0)] = norm[index_NL(N, n-1, 0)]*sqrt( (n-1.00)/(n-1.00/2.00) );
		for (int l = 1; l < n; ++l)
		{
			norm[index_NL(N, n, l)] = norm[index_NL(N, n, l-1)]/sqrt( k*(n-l) );
		}
	}
	return norm;
}

void fill_radial_harmonics_array_HarmOsc(double* R, double* R_norm, int N, float r, float k)
{
	//TODO change to normal M_PI
	const float PI = 3.14159265358979323846;
    float c;
    float rho = r*r/k;

    double* L;
    L = generate_laguerre_array_for_HarmOsc (N, rho, 0.5);

    c = sqrt( 2.00/sqrt(PI*k*k*k) )*exp(-rho/2.00);
    for (int n = 1; n <= N; ++n)
    {
    	double pow_r = 1;
        for(int l = 0; l < n; ++l)
        {
        	R[index_NL(N, n, l)] = c*pow_r*R_norm[index_NL(N, n, l)]*L[index_Laguerre(N, n-l-1, l)];
            pow_r *= r;
        }
    }
    free(L);
    return;
}

double* generate_spherical_norm(int N)
{
	double *Y_norm;
	Y_norm = (double*) calloc (N*(2*N - 1), sizeof(double));

    for(int l = 0; l < N; ++l)
    {
        double fact = 1.;
        double tmp0 = (2 * l + 1.)/(4. * M_PI);
        for(int m = 0; m <= l; ++m)
        {
        	if (m > 0) fact *= (l - m + 1) * (l + m);
            double tmp = sqrt(tmp0/fact);
            double sgn = (m % 2 == 0) ? (1) : (-1);
            Y_norm[index_LM(N, l,  m)] =         tmp;
            Y_norm[index_LM(N, l, -m)] =   sgn * tmp;
        }
    }
    return Y_norm;
}

void fill_assoc_Legendre_array(int N, double x, double* P)
{
    double y = sqrt(1.-x*x);

    P[index_LM(N, 0, 0)] = 1;//initialization
    if (N == 1) return;
    P[index_LM(N, 1, 0)] = x;
    for (int l = 2; l < N; ++l)  //First recursion for m=0 values.
    {
            P[index_LM(N,l,0)] = (2.*l-1.)/l*x*P[index_LM(N,(l-1),0)] - (l-1.)/l*P[index_LM(N,(l-2),0)];
    }

    for(int m = 1; m < N - 1; ++m)
    {
            P[index_LM(N, m, m)] = -1.*(2.*m-1.)*y*P[index_LM(N,(m-1),(m-1))]; //The P_m^m quantities
            P[index_LM(N,(m+1), m)] = (2.*m+1.)*x*P[index_LM(N,m,m)];//The P_m+1^m quantities.

            for (int l = m + 2; l < N; ++l)  //Recursion for the rest of P_l^m
            {
                    P[index_LM(N,l,m)] = (2.*l-1.)/(l-m)*x*P[index_LM(N,(l-1),m)] - (l+m-1.)/(l-m)*P[index_LM(N,(l-2),m)];
            }
    }

    P[index_LM(N,(N-1),(N-1))] = -1.*(2.*(N-1.)-1.)*y*P[index_LM(N,(N-2),(N-2))]; //The P_bw-1^(+/-)bw-1 quantities
    return;
}

double* generate_laguerre_array_for_HarmOsc(int N, float x, float alpha_min)
{
	double* L = (double*)malloc((N+1)*N/2*sizeof(double)); //The saved (outputed) Laguerre polynomials.

	float alpha;

	//First determine all L_0's ( n-l-1 = 0 for different l's)
	for (int l = 0; l < N; ++l)
	{
		L[index_Laguerre(N, 0, l)] = 1;
	}

	//And now all L_1's ( n-l-1 = 1 for l's up to and including N-2)
	for (int l = 0; l < N-1; ++l)
	{
		L[index_Laguerre(N, 1, l)] = -x + alpha_min + l + 1.0000;
	}

	for (int lower = 2; lower < N; ++lower)
	{
		for(int l=0; l < N - lower; ++l)
		{
			alpha = alpha_min + l;
			L[index_Laguerre(N, lower, l)] = 1.0000/lower *\
					(\
							(2.0000*lower-1.0000+alpha-x)*L[index_Laguerre(N, lower-1, l)]\
								   - (lower-1.0000+alpha)*L[index_Laguerre(N, lower-2, l)]\
					);
		}
	}
	return L;
}
////////////////////////////////////////////////////////////////////
// Some utilites That might not be needed, but are used at present.


///////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// Should get rid of this
