/******************************************************************************
Copyright (c) 2015 Dzmitry Padhorny, Andrey Kazennov and Dima Kozakov

Permission is hereby granted, free of charge, to any person obtaining a copy 
of this software and associated documentation files (the "Software"), to deal 
in the Software without restriction, including without limitation the rights 
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
SOFTWARE.
*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <math.h>
#include "rotational_matrix.h"

////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
// The important functions.

struct d_array* allocate_d_array(const int L)
{
	struct d_array* d;
	d = (struct d_array*) calloc (1, sizeof(struct d_array));
	d->L = L;
	d->data = (double*) calloc ((L+1)*(2*L + 1)*(2*L + 1), sizeof(double));
	return d;
}

struct many_d_arrays* allocate_many_d_arrays(const int L, const int num_angles)
{
	struct many_d_arrays* md;
	md = (struct many_d_arrays*) calloc (1, sizeof(struct many_d_arrays));
	md->L = L;
	md->num_angles = num_angles;
	md->data = (double*) calloc (num_angles*(L + 1)*(2*L + 1)*(2*L + 1), sizeof(double));
	return md;
}

struct D_array* allocate_D_array(const int L)
{
	struct D_array* D;
	D = (struct D_array*) calloc (1, sizeof(struct D_array));
	D->L = L;
	D->re = (double*) calloc ((L+1)*(2*L + 1)*(2*L + 1), sizeof(double));
	D->im = (double*) calloc ((L+1)*(2*L + 1)*(2*L + 1), sizeof(double));
	return D;
}

void deallocate_d_array(struct d_array* d)
{
	free(d->data);
	free(d);
}

void deallocate_many_d_arrays(struct many_d_arrays* md)
{
	free(md->data);
	free(md);
}

void deallocate_D_array(struct D_array* D)
{
	free(D->re);
	free(D->im);
	free(D);
}

struct d_array* generate_d_array(const int L, const double beta)
{
	struct d_array* d;
	d = allocate_d_array(L);
	/* Wolfram Mathematica*/

    d->data[index_d_array(L, 0,  0,  0)] =  1.0;
    d->data[index_d_array(L, 1, -1, -1)] =  1.0 * (1.0 + cos(beta)) / 2.0;
    d->data[index_d_array(L, 1, -1,  0)] =  1.0 * sin(beta) / sqrt(2.0);
    d->data[index_d_array(L, 1, -1,  1)] =  1.0 * (1.0 - cos(beta)) / 2.0;
    d->data[index_d_array(L, 1,  0, -1)] = -1.0 * sin(beta) / sqrt(2.0);
    d->data[index_d_array(L, 1,  0,  0)] =  1.0 * cos(beta);
    d->data[index_d_array(L, 1,  0,  1)] =  1.0 * sin(beta) / sqrt(2.0);
    d->data[index_d_array(L, 1,  1, -1)] =  1.0 * (1.0 - cos(beta)) / 2.0;
    d->data[index_d_array(L, 1,  1,  0)] = -1.0 * sin(beta) / sqrt(2.0);
    d->data[index_d_array(L, 1,  1,  1)] =  1.0 * (1.0 + cos(beta)) / 2.0;

    for(int l = 2; l <= L; ++l)
    {
    	for(int m = -l; m <= l; ++m)
    	{
    		double fact, fact1 = 1.0;
            //fact = sqrt( (2 * l + 1) / 2.);
	        fact=1;
            double d1 = fact;
            double d2 = fact;
            double d3 = fact;
            double d4 = fact;
            /* Eq. 26 Kostelec 2003 */
            for (int k = 1; k <= 2 * l; ++k)
            {
                    fact = sqrt(k / (((k <= l + m) ? k : 1.0) * ((k <= l - m) ? k : 1.0)));
                    fact1 *= fact;
                    d1 *= fact * ((k <= l + m) ? cos(beta / 2.0) : 1.0)* ((k <= l - m) ? -sin(beta / 2.0) : 1.0);
                    d2 *= fact * ((k <= l - m) ? cos(beta / 2.0) : 1.0)* ((k <= l + m) ?  sin(beta / 2.0) : 1.0);
                    d3 *= fact * ((k <= l + m) ? cos(beta / 2.0) : 1.0)* ((k <= l - m) ?  sin(beta / 2.0) : 1.0);
                    d4 *= fact * ((k <= l - m) ? cos(beta / 2.0) : 1.0)* ((k <= l + m) ? -sin(beta / 2.0) : 1.0);
            }

            d->data[index_d_array(L, l,  l,  m)] = d1;
            d->data[index_d_array(L, l, -l,  m)] = d2;
            d->data[index_d_array(L, l,  m,  l)] = d3;
            d->data[index_d_array(L, l,  m, -l)] = d4;

            for(int m1 = -l; m1 <=l; ++m1)
            {

                int j = l - 1;
                double val, val1;
                /* Eq. 28 Kostelec 2003 */
                if ((m1 > -l) && (m1 < l) && (m > -l) && (m < l))
                {
					val  = 1.0;
                    val *= (j + 1) * (2.0 * j + 1) / sqrt(((j + 1) * (j + 1) - m * m) * ((j + 1) * (j + 1) - m1 * m1));
                    val *= (cos(beta) - (double)(m * m1) / (double)(j * (j + 1))) * d->data[index_d_array(L, j, m, m1)];

					val1  = -1.0;
                    val1 *= sqrt((j * j - m * m) * (j * j - m1 * m1));
                    val1 *= (double)(j + 1.0) * d->data[index_d_array(L, j - 1, m, m1)] / (double)j;
                    val1 /= sqrt(((j + 1.0) * (j + 1.0) - (double)(m * m)) * ((j + 1.0) * (j + 1.0) - (double)(m1*m1)));

                    val += val1;
                    d->data[index_d_array(L, l, m, m1)] = val;
                }

            }
        }
    }

    return d;
}

void tab_many_d_arrays(const int L, const char *path)
{
    char filename[512];
    sprintf(filename, "%s/%s/%s_%d.dat", path, DFOLDER, DFILE_NEW, L);
    FILE* fp = fopen(filename, "w");
    printf("Start calculation of d's\n");
    for (int i = 0 ; i <= L; ++i)
    {
        printf("Calculating d for i = %d\n", i);
        //TODO 1) we don't sample the beta = pi angle this way .done
        //TODO 2) The frequency of sampling here is not equal to the one along alpha and gamma angles (pi/N against 2*pi/(2*N - 1))
        double beta = M_PI * (double)i / (double)(L);
        struct d_array *d;
        d = generate_d_array(L, beta);
        for(int l = 0; l <= L; ++l)
        {
            for(int m = -l; m <= l; ++m)
            {
                for(int m1 = -l; m1 <= l; ++m1)
                {
                  fprintf(fp, "%.15f\n", d->data[index_d_array(L, l, m, m1)]);
                }
            }
        }
        deallocate_d_array(d);
    }
    fclose(fp);
    return;
}

struct many_d_arrays* load_many_d_arrays(const int L, const char *path)
{
    char filename[512];
    sprintf(filename, "%s/%s/%s_%d.dat", path, DFOLDER, DFILE_NEW, L);
    struct many_d_arrays* md;
    md = allocate_many_d_arrays(L, L + 1);
    FILE* fp = fopen(filename, "r");
	if (fp == NULL) // file could not be opened
	{
		fprintf (stderr, "fopen error on %s:\n", path);
		perror ("fopen"), exit (EXIT_FAILURE);
	}
    for (int i = 0 ; i <= L; ++i)
    {
        for(int l = 0; l <= L; l++)
        {
            for(int m = -l; m <= l; m++)
            {
                for(int m1 = -l; m1 <= l; m1++)
                {
                  double tmp;
                  fscanf(fp, "%lf", &tmp);
                  md->data[index_many_d_arrays(L, i, l, m, m1)] = tmp;
                }
            }
        }
    }
    fclose(fp);

    return md;
}

struct many_d_arrays* get_many_d_arrays(const int L, const char *path)
{
	struct many_d_arrays* md;

    char d_file[512];
    sprintf(d_file, "%s/%s/%s_%d.dat", path, DFOLDER, DFILE_NEW, L);
    if (access(d_file, F_OK) == -1) // if file does not exist...
    {
        tab_many_d_arrays(L, path);
    }

    printf ("Loading d_small\n");
    md = load_many_d_arrays(L, path);

    return md;
}

struct many_d_arrays* get_many_d_arrays_new(const int num_angles, const int L)
{
	struct d_array *d;
    struct many_d_arrays* md;
    md = allocate_many_d_arrays(L, num_angles);
    for (int i = 0 ; i < num_angles; ++i)
    {
        double beta = M_PI * (double)i / (double)(num_angles - 1);
        d = generate_d_array(L, beta);
        for(int l = 0; l <= L; l++)
        {
            for(int m = -l; m <= l; m++)
            {
                for(int m1 = -l; m1 <= l; m1++)
                {
                  md->data[index_many_d_arrays(L, i, l, m, m1)] = d->data[index_d_array(L, l, m, m1)];
                }
            }
        }
    }
    deallocate_d_array(d);

    return md;
}

struct D_array* generate_D_array(const int L, const double alpha, const double beta, const double gamma)
{
	struct D_array* D;
	D = allocate_D_array(L);
	struct d_array* d;
	d = generate_d_array(L, beta);
	for (int l = 0; l <= L; ++l)
	{
		//printf("\nl = %d\n", l);
		for(int m = -l; m <= l; ++m)
		{
			for(int m1 = -l; m1 <= l; ++m1)
			{
				double dlmm1;
				dlmm1 = d->data[index_d_array(L, l, m, m1)];

				D->re[index_D_array(L, l, m, m1)] = cos(m * alpha + m1 * gamma) * dlmm1;
				D->im[index_D_array(L, l, m, m1)] = sin(m * alpha + m1 * gamma) * dlmm1;
            }
           // printf("\n");
		}
    }
	deallocate_d_array(d);

	return D;
}

struct spf_coeffs* rotate_spf_coeffs(const struct spf_coeffs* coeffs, const struct D_array* D)
{
	int N = coeffs->N;
	int L = N - 1;
	struct spf_coeffs* rot_coeffs;
	rot_coeffs = allocate_spf_coeffs(N);

    for (int n = 1; n <= N; ++n)
    {
        for(int l = 0; l <= L; ++l)
        {
        	for(int m = -l; m <= l; ++m)
        	{
        		double re = 0.;
        		double im = 0.;
        		for(int m1 = -l; m1 <= l; ++m1)
        		{
        			double Dr = D->re[index_D_array(L, l, m, m1)];
                    double Di = D->im[index_D_array(L, l, m, m1)];

                    re +=  Dr * coeffs->re[index_spf_coeffs(N, n, l, m1)] - Di * coeffs->im[index_spf_coeffs(N, n, l, m1)];
                    im +=  Dr * coeffs->im[index_spf_coeffs(N, n, l, m1)] + Di * coeffs->re[index_spf_coeffs(N, n, l, m1)];
        		}
        		rot_coeffs->re[index_spf_coeffs(N, n, l, m)] = re;
        		rot_coeffs->im[index_spf_coeffs(N, n, l, m)] = im;
        	}
        }
    }

    return rot_coeffs;
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
////////////////////////////////////////////////////////////////////
// Some utilites That might not be needed, but are used at present.
void calc_delta(struct matrix_3d* delta, const int L)
{
	int l, m1, m2;
	/*
	Delta is matrix L x 2L x 2L
	l  in [0, L]
	m1 in [-L, L]
	m2 in [-L, L]
	so
	m1 = l + m1
	m2 = l + m2
	*/

	/*Firstly calc triangle [0,l][0, m1<=m2][0, m2]*/
	// lm1m2
	set_matrix_3d(delta,0,L,L,1);
	for (l = 1; l <= L; l++)
	{
		//printf("Calc delta for l = %d\n", l);
		set_matrix_3d(delta,l,l + L, L, sqrt((2. * l - 1) / (2. * l)) * get_matrix_3d(delta, l - 1, L + l - 1, L));

		for(m2 = 0; m2 <= l; m2++)
		{
			double val;
			if (m2 !=0)
			{
				val = sqrt(l * (2. * l - 1) / (2. * (l + m2) * (l + m2 - 1))) * get_matrix_3d(delta, l - 1, L + l - 1, L + m2 - 1);
				set_matrix_3d(delta,l, l + L, m2 + L, val);
			}

			for(m1 = l - 1; (m1 >= m2) ; m1--)
			{
				//if (l ==1 && m2 = 0 & m1==1)printf("%d %d %d %f\n", l, m2, m1, get_matrix_3d(delta, l, L + m1, L + m2));
				val = -2. * m2 * get_matrix_3d(delta, l, L + m1 + 1, L + m2) / sqrt((l - m1)*(l + m1 + 1));
				val -= sqrt((l - m1 - 1.) * (l + m1 + 2.) / ((l - m1) * (l + m1 + 1.))) * get_matrix_3d(delta, l, L + m1 + 2, L + m2);
				set_matrix_3d(delta,l, m1 + L, m2 + L, val);

				//printf("%d %d %d %f\n", l, m2, m1, get_matrix_3d(delta, l, L + m1, L + m2));
			}
		}
	}
	/*Secondly calc other triangles*/
	for (l = 0; l <= L; l++)
	{
		for(m2 = 0; m2 <= l; m2++)
		{
			for(m1 = m2 ; (m1 <= l); m1++)
			{
				double d_lm1m2 = get_matrix_3d(delta, l, L + m1, L + m2);
				//lm1-m2
				set_matrix_3d(delta, l, L + m1, L - m2, pow(-1., l - m1) * d_lm1m2);
				//l-m1m2
				set_matrix_3d(delta, l, L - m1, L + m2, pow(-1., l - m2) * d_lm1m2);
				//lm2m1
				set_matrix_3d(delta, l, L + m2, L + m1, pow(-1., m1 - m2) * d_lm1m2);
				//l-m1-m2
				set_matrix_3d(delta, l, L - m1, L - m2, pow(-1., m1 - m2) * d_lm1m2);
				//lm2-m1
				set_matrix_3d(delta, l, L + m2, L - m1, pow(-1., l - m1) * d_lm1m2);
				//l-m2m1
				set_matrix_3d(delta, l, L - m2, L + m1, pow(-1., l - m2) * d_lm1m2);
				//l-m2-m1
				set_matrix_3d(delta, l, L - m2, L - m1, d_lm1m2);

			}
		}

	}
/*
	for(l = 25; l <=25; l++)
	{
		printf("\nl = %d\n", l);
		for(m1 = -l; m1 <= l; m1++)
		{
			for(m2 = -l; m2 <= l; m2++)
			{
				printf ("%.7E\n", get_matrix_3d(delta, l, L + m1, L + m2));
			}
		 //   printf ("\n");
		}
	}
*/
}

double**  create_rotation_matrix(double alpha, double beta, double gamma)
{
	double** rm;
	rm = (double**)calloc(3, sizeof(double*));
	rm[0] = (double*)calloc(3, sizeof(double));
	rm[1] = (double*)calloc(3, sizeof(double));
	rm[2] = (double*)calloc(3, sizeof(double));
	rm[0][0] = cos(gamma)*cos(beta)*cos(alpha) - sin(gamma)*sin(alpha);
	rm[0][1] = cos(gamma)*cos(beta)*sin(alpha) + sin(gamma)*cos(alpha);
	rm[0][2] = -cos(gamma)*sin(beta);
	rm[1][0] = -sin(gamma)*cos(beta)*cos(alpha) - cos(gamma)*sin(alpha);
	rm[1][1] = -sin(gamma)*cos(beta)*sin(alpha) + cos(gamma)*cos(alpha);
	rm[1][2] = sin(gamma)*sin(beta);
	rm[2][0] = sin(beta)*cos(alpha);
	rm[2][1] = sin(beta)*sin(alpha);
	rm[2][2] = cos(beta);
//	{ cos(g)*cos(b)*cos(a) - sin(g)*sin(a),  cos(g)*cos(b)*sin(a) + sin(g)*cos(a), -cos(g)*sin(b)},
//	{-sin(g)*cos(b)*cos(a) - cos(g)*sin(a), -sin(g)*cos(b)*sin(a) + cos(g)*cos(a),  sin(g)*sin(b)},
//	{ sin(b)*cos(a)                       ,  sin(b)*sin(a)                       ,  cos(b)       }
	return rm;
}

void fill_active_rotation_matrix(double rm[3][3], double alpha, double beta, double gamma)
{
	rm[0][0] = cos(gamma)*cos(beta)*cos(alpha) - sin(gamma)*sin(alpha);
	rm[1][0] = cos(gamma)*cos(beta)*sin(alpha) + sin(gamma)*cos(alpha);
	rm[2][0] = -cos(gamma)*sin(beta);
	rm[0][1] = -sin(gamma)*cos(beta)*cos(alpha) - cos(gamma)*sin(alpha);
	rm[1][1] = -sin(gamma)*cos(beta)*sin(alpha) + cos(gamma)*cos(alpha);
	rm[2][1] = sin(gamma)*sin(beta);
	rm[0][2] = sin(beta)*cos(alpha);
	rm[1][2] = sin(beta)*sin(alpha);
	rm[2][2] = cos(beta);
//	{ cos(g)*cos(b)*cos(a) - sin(g)*sin(a),  cos(g)*cos(b)*sin(a) + sin(g)*cos(a), -cos(g)*sin(b)},
//	{-sin(g)*cos(b)*cos(a) - cos(g)*sin(a), -sin(g)*cos(b)*sin(a) + cos(g)*cos(a),  sin(g)*sin(b)},
//	{ sin(b)*cos(a)                       ,  sin(b)*sin(a)                       ,  cos(b)       }
}

void fill_passive_rotation_matrix(double rm[3][3], double alpha, double beta, double gamma)
{
	rm[0][0] = cos(gamma)*cos(beta)*cos(alpha) - sin(gamma)*sin(alpha);
	rm[0][1] = cos(gamma)*cos(beta)*sin(alpha) + sin(gamma)*cos(alpha);
	rm[0][2] = -cos(gamma)*sin(beta);
	rm[1][0] = -sin(gamma)*cos(beta)*cos(alpha) - cos(gamma)*sin(alpha);
	rm[1][1] = -sin(gamma)*cos(beta)*sin(alpha) + cos(gamma)*cos(alpha);
	rm[1][2] = sin(gamma)*sin(beta);
	rm[2][0] = sin(beta)*cos(alpha);
	rm[2][1] = sin(beta)*sin(alpha);
	rm[2][2] = cos(beta);
//	{ cos(g)*cos(b)*cos(a) - sin(g)*sin(a),  cos(g)*cos(b)*sin(a) + sin(g)*cos(a), -cos(g)*sin(b)},
//	{-sin(g)*cos(b)*cos(a) - cos(g)*sin(a), -sin(g)*cos(b)*sin(a) + cos(g)*cos(a),  sin(g)*sin(b)},
//	{ sin(b)*cos(a)                       ,  sin(b)*sin(a)                       ,  cos(b)       }
}

///////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// Should get rid of this

/*** MATRIX 3D BASIC ***/

// allocate 3D matrix n*n*n
struct matrix_3d* allocate_matrix_3d(int n)
{
	struct matrix_3d* M;
	M = (struct matrix_3d*)malloc(sizeof(struct matrix_3d));
	M->n = n;
	M->data = (double*)calloc(n*n*n, sizeof(double));
	return M;
}

void free_matrix_3d(struct matrix_3d* M)
{
	free(M->data);
	free(M);
}

// get index in linear array from zero-based indices
size_t zindex_matrix_3d(const struct matrix_3d* M, int n, int l, int m)
{
	// TODO: Change indexing for nice caching
	return (M->n * (M->n * (n) + l) + m);
}

// For 3d matrices is the same as zindex_matrix_3d
size_t index_matrix_3d(const struct matrix_3d* M, int n, int l, int m)
{
	return zindex_matrix_3d(M,n,l,m);
}

// get matrix element
double get_matrix_3d(const struct matrix_3d* M, int n, int l, int m)
{
	return M->data[index_matrix_3d(M,n,l,m)];
}

// set matrix element
void set_matrix_3d(struct matrix_3d* M, int n, int l, int m, const double val)
{
	M->data[index_matrix_3d(M,n,l,m)] = val;
}
#endif // DOXYGEN_SHOULD_SKIP_THIS
