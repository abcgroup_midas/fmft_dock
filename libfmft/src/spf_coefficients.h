/******************************************************************************
Copyright (c) 2015 Dzmitry Padhorny, Andrey Kazennov and Dima Kozakov

Permission is hereby granted, free of charge, to any person obtaining a copy 
of this software and associated documentation files (the "Software"), to deal 
in the Software without restriction, including without limitation the rights 
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
SOFTWARE.
*******************************************************************************/
/** \file spf_coefficients.h
	This file contains structures and functions
	that are used in creating SPF-based
	representation of the protein.
*/
#pragma once
#include <errno.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "index.h"
#include "polynomials.h"
#include "simple_functions.h"


////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
// The important functions.
/**
	Mysterious structure
*/
struct vector3
{
        float X; /**< translation in the X dimension */
        float Y; /**< translation in the Y dimension */
        float Z; /**< translation in the Z dimension */
};

/**
 * This structure is used for storing
 * spf coefficients calculated up to polynomial order N.
 * Individual spf coefficients should be accessed
 * using index_spf_coeffs() inline indexing function.
 */
struct spf_coeffs
{
	int N; /**< Depth of SPF transform (Highest polynomial order of coefficients). */
	double *re; /**< Array of size N*N*(2*N - 1)*/
	double *im; /**< Array of size N*N*(2*N - 1)*/
};

/**
 * This structure is used for storing
 * spf coefficients for a set of pairs of
 * corresponding grids. N is the highest polynomial order
 * for which they were calculated.
 * Individual spf coefficients should be accessed
 * using index_many_spf_coeffs() inline indexing function.
 */
struct many_spf_coeffs
{
	int N; /**< Depth of SPF transform. */
	int count; /**< Depth of SPF transform. */
	struct vector3* rec_origin; /**< origin of generalized fft transform of receptor. */
	struct vector3* lig_origin; /**< origin of generalized fft transform of ligand. */
	double *ar; /**< Array of size count*N*N*(2*N - 1)*/
	double *ai; /**< Array of size count*N*N*(2*N - 1)*/
	double *br; /**< Array of size count*N*N*(2*N - 1)*/
	double *bi; /**< Array of size count*N*N*(2*N - 1)*/
};

/**
 * This function is used for indexing
 * arrays holding spf_coeffitients.
 * \param N Highest polynomial order, for which the coefficients were calculated.
 * \param n In range [1, N].
 * \param l In range [0, n-1].
 * \param m In range [-l, l].
 */
inline int index_spf_coeffs(int N, int n, int l, int m)
{
	return N*(N*(m + (N - 1)) + l) + n - 1;
}

/**
 * This function is used for indexing
 * arrays holding spf_coeffitients for pairs of corresponding grids.
 * \param N Highest polynomial order, for which the coefficients were calculated
 * \param gcount Number of grid pairs
 * \param p In range [0, gcount-1].
 * \param n In range [1, N].
 * \param l In range [0, n-1].
 * \param m In range [-l, l].
 */
inline int index_many_spf_coeffs(int N, int gcount, int p, int n, int l, int m)
{
	return gcount*(N*(N*(m + (N - 1)) + l) + n - 1) + p;
}


/**
	Allocates an instance of spf_coeffs struct
	suitable for containing spf coeffitients up to polynomial order N.
	\param N Highest spf coefficients' polynomial order
	\return Pointer to spf_coeffs struct
 */
struct spf_coeffs* allocate_spf_coeffs(int N);

struct many_spf_coeffs* allocate_many_spf_coeffs(int N, int grids_count);

/**
	Deallocates an instance of spf_coeffs struct;
 */
void deallocate_spf_coeffs(struct spf_coeffs* coeffs);


/**
	Deallocates an instance of many_spf_coeffs struct;
 */
void deallocate_many_spf_coeffs(struct many_spf_coeffs* mcoeffs);


/**
 *	Creates an instance of spf_coeffs struct an fills it
 *	according to the insides of file filename.
 *	\param filename Path to the file containing spf coefficients.
 *	The first line must contain SPF depth, all consequent lines
 *	must contain real an imaginary parts of an appropriate spf coefficient.
 */
struct spf_coeffs* read_spf_coeffs(const char *filename);

/**
 * Writes a set of spf_coefficiets held by an instance of spf_coeffs struct
 * into a file specified by filename parameter.
 * The first line of the file will contain SPF depth N,
 * all consequent lines will contain real and imaginary parts of
 * appropriate spf coefficients. (i.e 2nd line holds real and imaginary parts of n=1, l=0, m=0 element, 3d line - n=2, l=0, m=0. 4th - n=2, l=1, m=-1 and so on)
 * \param coeffs Pointer to spf_coeffs struct.
 * \param filename Path to a file to be created.
 */
void print_spf_coeffs(const struct spf_coeffs* coeffs, const char *filename);

struct many_spf_coeffs* read_many_spf_coeffs(const char *filename);

struct many_spf_coeffs* get_many_spf_coeffs_low_order(const struct many_spf_coeffs* mcoeffs, const int N);

void print_many_spf_coeffs(const struct many_spf_coeffs* mcoeffs, const char *filename);

struct many_spf_coeffs* scale_many_spf_coeffs(const struct many_spf_coeffs* mcoeffs, struct weights* wset);

/**
	This structure holds a set of weights for components of scoring function.
*/
struct weights
{
	int n;	 /**< Number of weights in a set */
	int label; /**< index of weight set*/
	double* vals; /**< A pointer to an array of weight values */
};

/**
	Holds several weight sets;
*/
struct many_weights
{
	int count; /**< Number of weight sets */
	struct weights** wset; /**< A pointer to an array of weight sets */
};

struct weights* allocate_weights(int n);

struct many_weights* allocate_many_weights(int count);

void deallocate_weights(struct weights* wset);

void deallocate_many_weights(struct many_weights* mweights);

struct many_weights* read_many_weights(char* path);


////////////////////////////////////////////////////////////////////
// Some utilites That might not be needed, but are used at present.

unsigned int count_lines (FILE *file);

unsigned int count_words (const char string[ ]);
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
// Should get rid of this
