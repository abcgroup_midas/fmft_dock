/******************************************************************************
Copyright (c) 2015 Dzmitry Padhorny, Andrey Kazennov and Dima Kozakov

Permission is hereby granted, free of charge, to any person obtaining a copy 
of this software and associated documentation files (the "Software"), to deal 
in the Software without restriction, including without limitation the rights 
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
SOFTWARE.
*******************************************************************************/
#include "spf_coefficients.h"

////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
// The important functions.
struct spf_coeffs* allocate_spf_coeffs(int N)
{
	struct spf_coeffs *coeffs = (struct spf_coeffs*) calloc (1, sizeof (struct spf_coeffs));
	coeffs->N = N;
	coeffs->re = (double*) calloc (N*N*(2*N - 1), sizeof (double));
	coeffs->im = (double*) calloc (N*N*(2*N - 1), sizeof (double));
	return coeffs;
}

struct many_spf_coeffs* allocate_many_spf_coeffs(int N, int grids_count)
{
	struct many_spf_coeffs* many_coeffs;
	many_coeffs = (struct many_spf_coeffs*) calloc (1, sizeof (struct many_spf_coeffs));
	many_coeffs->N = N;
	many_coeffs->count = grids_count;
	many_coeffs->rec_origin = (struct vector3*) calloc (1, sizeof (struct vector3));
	many_coeffs->lig_origin = (struct vector3*) calloc (1, sizeof (struct vector3));
	many_coeffs->ar = (double*) calloc (N*N*(2*N - 1)*grids_count, sizeof (double));
	many_coeffs->ai = (double*) calloc (N*N*(2*N - 1)*grids_count, sizeof (double));
	many_coeffs->br = (double*) calloc (N*N*(2*N - 1)*grids_count, sizeof (double));
	many_coeffs->bi = (double*) calloc (N*N*(2*N - 1)*grids_count, sizeof (double));
	return many_coeffs;
}

void deallocate_spf_coeffs(struct spf_coeffs* coeffs)
{
	free(coeffs->re);
	free(coeffs->im);
	free(coeffs);
}

void deallocate_many_spf_coeffs(struct many_spf_coeffs* mcoeffs)
{
	free(mcoeffs->ar);
	free(mcoeffs->ai);
	free(mcoeffs->br);
	free(mcoeffs->bi);
	free(mcoeffs);
}

struct spf_coeffs* read_spf_coeffs(const char *filename)
{
	int N;
	struct spf_coeffs* coeffs;
    FILE *fp;
    if ( (fp = fopen (filename,"r")) == NULL)
    {
    	fprintf (stderr, "File %s could not be opened.\n", filename);
        exit (EXIT_FAILURE);
    }
    else
    {
    	fscanf(fp, "%d", &N);
    	coeffs = allocate_spf_coeffs (N);

    	for(int n = 1; n <= N; ++n)
    	{
    		for(int l = 0; l < n; ++l)
    		{
    			for(int m = -l; m <= l; ++m)
    			{
    				if( fscanf (fp, "%lf %lf", &coeffs->re[index_spf_coeffs(N, n, l, m)], &coeffs->im[index_spf_coeffs(N, n, l, m)]) != 2 )
    				{
    					printf ("Read error on file %s\n", filename);
    					exit (EXIT_FAILURE);
    				}
    			}
    		}
    	}
    }
    fclose(fp);

    return coeffs;
}

void print_spf_coeffs(const struct spf_coeffs* coeffs, const char *filename)
{
	int N = coeffs->N;
    FILE *fp;
    if ( (fp = fopen (filename,"w")) == NULL)
    {
    	fprintf (stderr, "File %s could not be opened.\n", filename);
        exit (EXIT_FAILURE);
    }
    else
    {
    	fprintf (fp, "%d\n", coeffs->N);

    	for(int n = 1; n <= N; ++n)
    	{
    		for(int l = 0; l < n; ++l)
    		{
    			for(int m = -l; m <= l; ++m)
    			{
    				fprintf (fp, "%.15lf %.15lf\n", coeffs->re[index_spf_coeffs(N, n, l, m)], coeffs->im[index_spf_coeffs(N, n, l, m)]);
    			}
    		}
    	}
    }
    fclose(fp);
}

struct many_spf_coeffs* read_many_spf_coeffs(const char *filename)
{
	struct many_spf_coeffs* mcoeffs;
	int N;
	int count;
	double rec_X, rec_Y, rec_Z;
	double lig_X, lig_Y, lig_Z;
    FILE *fp;
    if ( (fp = fopen (filename,"r")) == NULL)
    {
    	fprintf (stderr, "File %s could not be opened.\n", filename);
        exit (EXIT_FAILURE);
    }
    else
    {
    	if(fscanf(fp, "%d %d\n", &N, &count) != 2)
    	{
        	fprintf (stderr, "Error on reading file %s.\n", filename);
            exit (EXIT_FAILURE);
    	}
    	if(fscanf(fp, "%lf %lf %lf\n", &rec_X, &rec_Y, &rec_Z) != 3)
    	{
        	fprintf (stderr, "Error on reading file %s.\n", filename);
            exit (EXIT_FAILURE);
    	}
    	if(fscanf(fp, "%lf %lf %lf\n", &lig_X, &lig_Y, &lig_Z) != 3)
    	{
        	fprintf (stderr, "Error on reading file %s.\n", filename);
            exit (EXIT_FAILURE);
    	}
    	mcoeffs = allocate_many_spf_coeffs(N, count);

    	mcoeffs->rec_origin->X = rec_X;
    	mcoeffs->rec_origin->Y = rec_Y;
    	mcoeffs->rec_origin->Z = rec_Z;

    	mcoeffs->lig_origin->X = lig_X;
    	mcoeffs->lig_origin->Y = lig_Y;
    	mcoeffs->lig_origin->Z = lig_Z;

    	for(int p = 0; p < mcoeffs->count; ++p)
    	{
    		for(int n = 1; n <= mcoeffs->N; ++n)
    		{
    			for(int l = 0; l < n; ++l)
    			{
    				for(int m = -l; m <= l; ++m)
    				{
    					if(4 != fscanf(fp, "%lf %lf %lf %lf\n", &mcoeffs->ar[index_many_spf_coeffs(mcoeffs->N, mcoeffs->count, p, n, l, m)],\
    																	&mcoeffs->ai[index_many_spf_coeffs(mcoeffs->N, mcoeffs->count, p, n, l, m)],\
    																	&mcoeffs->br[index_many_spf_coeffs(mcoeffs->N, mcoeffs->count, p, n, l, m)],\
    																	&mcoeffs->bi[index_many_spf_coeffs(mcoeffs->N, mcoeffs->count, p, n, l, m)]))
    					{
    			        	fprintf (stderr, "Error on reading file %s.\n", filename);
    			            exit (EXIT_FAILURE);
    					}
    				}
    			}
    		}
    	}
    }
    fclose(fp);

    return mcoeffs;
}

struct many_spf_coeffs* get_many_spf_coeffs_low_order(const struct many_spf_coeffs* mcoeffs, int N)
{
	if (N > mcoeffs->N)
	{
		fprintf(stderr, "Polinomial order if the output set can't be higher than that of the input set");
		exit(1);
	}

	struct many_spf_coeffs* new_mcoeffs;
	new_mcoeffs = allocate_many_spf_coeffs(N, mcoeffs->count);

	new_mcoeffs->rec_origin->X = mcoeffs->rec_origin->X;
	new_mcoeffs->rec_origin->Y = mcoeffs->rec_origin->Y;
	new_mcoeffs->rec_origin->Z = mcoeffs->rec_origin->Z;

	new_mcoeffs->lig_origin->X = mcoeffs->lig_origin->X;
	new_mcoeffs->lig_origin->Y = mcoeffs->lig_origin->Y;
	new_mcoeffs->lig_origin->Z = mcoeffs->lig_origin->Z;

	for(int p = 0; p < mcoeffs->count; ++p)
	{
		for(int n = 1; n <= new_mcoeffs->N; ++n)
		{
			for(int l = 0; l < n; ++l)
			{
				for(int m = -l; m <= l; ++m)
				{
					new_mcoeffs->ar[index_many_spf_coeffs(new_mcoeffs->N, new_mcoeffs->count, p, n, l, m)] =\
							mcoeffs->ar[index_many_spf_coeffs(mcoeffs->N, mcoeffs->count, p, n, l, m)];
					new_mcoeffs->ai[index_many_spf_coeffs(new_mcoeffs->N, new_mcoeffs->count, p, n, l, m)] =\
							mcoeffs->ai[index_many_spf_coeffs(mcoeffs->N, mcoeffs->count, p, n, l, m)];
					new_mcoeffs->br[index_many_spf_coeffs(new_mcoeffs->N, new_mcoeffs->count, p, n, l, m)] =\
							mcoeffs->br[index_many_spf_coeffs(mcoeffs->N, mcoeffs->count, p, n, l, m)];
					new_mcoeffs->bi[index_many_spf_coeffs(new_mcoeffs->N, new_mcoeffs->count, p, n, l, m)] =\
							mcoeffs->bi[index_many_spf_coeffs(mcoeffs->N, mcoeffs->count, p, n, l, m)];
				}
			}
		}
	}

	return new_mcoeffs;
}

void print_many_spf_coeffs(const struct many_spf_coeffs* mcoeffs, const char *filename)
{
	int N = mcoeffs->N;
    FILE *fp;
    if ( (fp = fopen (filename,"w")) == NULL)
    {
    	fprintf (stderr, "File %s could not be opened.\n", filename);
        exit (EXIT_FAILURE);
    }
    else
    {
    	fprintf (fp, "%d %d\n", mcoeffs->N, mcoeffs->count);
    	fprintf (fp, "%.15E %.15E %.15E\n", mcoeffs->rec_origin->X, mcoeffs->rec_origin->Y, mcoeffs->rec_origin->Z);
    	fprintf (fp, "%.15E %.15E %.15E\n", mcoeffs->lig_origin->X, mcoeffs->lig_origin->Y, mcoeffs->lig_origin->Z);

    	for(int p = 0; p < mcoeffs->count; ++p)
    	{
    		for(int n = 1; n <= N; ++n)
    		{
    			for(int l = 0; l < n; ++l)
    			{
    				for(int m = -l; m <= l; ++m)
    				{
    					fprintf (fp, "%.15E %.15E %.15E %.15E\n", mcoeffs->ar[index_many_spf_coeffs(N, mcoeffs->count, p, n, l, m)],\
    																	mcoeffs->ai[index_many_spf_coeffs(N, mcoeffs->count, p, n, l, m)],\
    																	mcoeffs->br[index_many_spf_coeffs(N, mcoeffs->count, p, n, l, m)],\
    																	mcoeffs->bi[index_many_spf_coeffs(N, mcoeffs->count, p, n, l, m)]);
    				}
    			}
    		}
    	}
    }
    fclose(fp);

}

struct many_spf_coeffs* scale_many_spf_coeffs(const struct many_spf_coeffs* mcoeffs, struct weights* wset)
{
	if (wset->n != mcoeffs->count)
	{
		fprintf(stderr, "The number of weights in a set is different form the number of coefficient sets");
		exit(0);
	}

	struct many_spf_coeffs* new_mcoeffs;
	new_mcoeffs = allocate_many_spf_coeffs(mcoeffs->N, mcoeffs->count);

	new_mcoeffs->rec_origin->X = mcoeffs->rec_origin->X;
	new_mcoeffs->rec_origin->Y = mcoeffs->rec_origin->Y;
	new_mcoeffs->rec_origin->Z = mcoeffs->rec_origin->Z;

	new_mcoeffs->lig_origin->X = mcoeffs->lig_origin->X;
	new_mcoeffs->lig_origin->Y = mcoeffs->lig_origin->Y;
	new_mcoeffs->lig_origin->Z = mcoeffs->lig_origin->Z;


	for(int p = 0; p < mcoeffs->count; ++p)
	{
		for(int n = 1; n <= new_mcoeffs->N; ++n)
		{
			for(int l = 0; l < n; ++l)
			{
				for(int m = -l; m <= l; ++m)
				{
					new_mcoeffs->ar[index_many_spf_coeffs(new_mcoeffs->N, new_mcoeffs->count, p, n, l, m)] =\
							mcoeffs->ar[index_many_spf_coeffs(mcoeffs->N, mcoeffs->count, p, n, l, m)] *\
							wset->vals[p];
					new_mcoeffs->ai[index_many_spf_coeffs(new_mcoeffs->N, new_mcoeffs->count, p, n, l, m)] =\
							mcoeffs->ai[index_many_spf_coeffs(mcoeffs->N, mcoeffs->count, p, n, l, m)]*\
							wset->vals[p];
					new_mcoeffs->br[index_many_spf_coeffs(new_mcoeffs->N, new_mcoeffs->count, p, n, l, m)] =\
							mcoeffs->br[index_many_spf_coeffs(mcoeffs->N, mcoeffs->count, p, n, l, m)];
					new_mcoeffs->bi[index_many_spf_coeffs(new_mcoeffs->N, new_mcoeffs->count, p, n, l, m)] =\
							mcoeffs->bi[index_many_spf_coeffs(mcoeffs->N, mcoeffs->count, p, n, l, m)];
				}
			}
		}
	}

	return new_mcoeffs;
}


struct weights* allocate_weights(int n)
{
	struct weights* wset = (struct weights*) calloc(1, sizeof(struct weights));
	wset->n = n;
	wset->vals = (double*) calloc(n, sizeof(double));

	return wset;
}

struct many_weights* allocate_many_weights(int count)
{
	struct many_weights* mweights = (struct many_weights*) calloc(1, sizeof(many_weights));
	mweights->count = count;
	mweights->wset = (struct weights**) calloc(count, sizeof(struct weights*));

	return mweights;
}

void deallocate_weights(struct weights* wset)
{
	free(wset->vals);
	free(wset);
}

void deallocate_many_weights(struct many_weights* mweights)
{
	for (int i = 0; i < mweights->count; ++i)
	{
		deallocate_weights(mweights->wset[i]);
	}
	free(mweights->wset);
	free(mweights);
}

struct many_weights* read_many_weights(char* path)
{
	int count, n;
	FILE* f = fopen (path, "r");
	if (f == NULL)
	{
		fprintf (stderr, "couldn't open file %s\nExiting.", path);
		exit (1);
	}
	// get number of coefficient sets
	count = count_lines (f);
	struct many_weights* mweights = allocate_many_weights (count);

	//read file line by line
	char line[1024];
	//read weight sets, one by one
	for (int i = 0; i < mweights->count; ++i)
	{
		fgets (line, 1023, f);
		n = ( count_words (line) - 1);
		mweights->wset[i] = allocate_weights (n);

		//read set label
		char * pch;
		pch = strtok (line, " \t\n");
		mweights->wset[i]->label = atoi (pch);
		//read coefficients one by one
		for (int j = 0; j < mweights->wset[i]->n; ++j)
		{
			pch = strtok (NULL, " \t\n");
			mweights->wset[i]->vals[j] = atof (pch);
		}
	}

	return mweights;
}

////////////////////////////////////////////////////////////////////
// Some utilites That might not be needed, but are used at present.
//TODO make it count only non-emoty lines
unsigned int count_lines (FILE *file)
{
	unsigned int lines = 0;
	int c = '\0';
	int pc = '\n';

	while (c = fgetc (file), c != EOF)
	{
		if (c == '\n' && pc != '\n')
			lines++;
		pc = c;
	}
	if (pc != '\n')
		lines++;

	rewind(file);

	return lines;
}

unsigned int count_words (const char string[ ])
{
    int nwords = 0;

    // state:
    const char* it = string;
    int inword = 0;
    int line_end = 0;

    do switch (*it)
    {
        case '\0': case '\n':
            if (inword)
            {
            	inword = 0;
            	nwords++;
            }
            line_end = 1;
            break;
        case ' ': case '\t': // TODO others?
            if (inword)
            {
            	inword = 0;
            	nwords++;
            }
            break;
        default: inword = 1;
    } while (*it++ && line_end == 0);

    return nwords;
}
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
// Should get rid of this
