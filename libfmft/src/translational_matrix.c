/******************************************************************************
Copyright (c) 2015 Dzmitry Padhorny, Andrey Kazennov and Dima Kozakov

Permission is hereby granted, free of charge, to any person obtaining a copy 
of this software and associated documentation files (the "Software"), to deal 
in the Software without restriction, including without limitation the rights 
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
SOFTWARE.
*******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <math.h>
#include "translational_matrix.h"

////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
// The important functions.
int min(int i, int j)
{
	if(i<j)
		return i;
	else
		return j;
}
int max(int i, int j)
{
	if(i>j)
		return i;
	else
		return j;
}
//
//	Arbitrary precision functions and stuff
//

//TODO find out if it is overloaded by default
mpf_class pow(const mpf_class &a, const int n)
{
	mpf_t rop;
	mpf_init(rop);
	mpf_pow_ui(rop, a.get_mpf_t(), n);
	mpf_class retval(rop);
	mpf_clear(rop);
	return retval;
}

struct T_array_arb* allocate_T_array_arb(const int N)
{
	struct T_array_arb* T_arb = (struct T_array_arb*) calloc (1, sizeof(struct T_array_arb));
	T_arb->N = N;
	T_arb->data = new fpval[N*N*N*N*N];
	return T_arb;
}

void deallocate_T_array_arb(struct T_array_arb* T_arb)
{
	delete[] T_arb->data;
	free(T_arb);
}

struct A_array_arb* allocate_A_array_arb(const int N)
{
	struct A_array_arb* A_arb = (struct A_array_arb*) calloc (1, sizeof(struct A_array_arb));
	A_arb->N = N;
	A_arb->data = new fpval[2*N*N*N*N];
	return A_arb;
}

void deallocate_A_array_arb(struct A_array_arb* A_arb)
{
	delete[] A_arb->data;
	free(A_arb);
}

fpval* generate_desc_fact_array_arb(const int n)
{
    fpval *desc_fact = new fpval[n+1];
    desc_fact[0] = 1;
    for (int i = 1; i <= n; ++i)
    {
    	desc_fact[i] = desc_fact[i-1] * i;
    }

    return desc_fact;
}

fpval* generate_rise_fact_array_arb(const fpval x, const int n)
{
    fpval *rise_fact = new fpval[n+1];
    //TODO Find out if rising fatorial really defined for i = 0;
    rise_fact[0] = x;
    if(n > 0)
    {
    	rise_fact[1] = x;
    }
    for (int i = 2; i <= n; ++i)
    {
    	rise_fact[i] = (x + i - 1)*rise_fact[i-1];
    }

    return rise_fact;
}

//TODO Decide if needed
fpval X_arb(const int n, const int l, const int j, const fpval* desc_fact, const fpval* rise_fact_half)
{
	fpval one;
	if((n-l-j-1)%2 == 0)
	{
		one = 1;
	}
	else
	{
		one = -1;
	}
	////fpval result = gmp_factorial(n-l-1);
	//fpval result = sqrt(descend_factorial1(desc, n-l-1)*rising_factorial1(rise, 0.5, n)*0.5);
	//result *= one/(descend_factorial1(desc, j)*descend_factorial1(desc, n-l-j-1)*rising_factorial1(rise, 0.5, l+j+1));
	fpval result = sqrt(desc_fact[n-l-1]*rise_fact_half[n]*0.5);
	result *= one/(desc_fact[j]*desc_fact[n-l-j-1]*rise_fact_half[l+j+1]);
	return result;
}

fpval* generate_X_array_arb(const int N, const fpval* desc_fact, const fpval* rise_fact_half)
{
	fpval* X_tab;
	X_tab = new fpval[N*N*N];
    for(int n = 1; n <= N; ++n)
    {
    	for(int l = 0; l < n; ++l)
    	{
    		for(int j = 0; j <= n-l-1; j++)
    		{
    			X_tab[((n - 1)*N + l)*N + j] = X_arb(n, l, j, desc_fact, rise_fact_half);
    		}
    	}
    }
    return X_tab;
}

fpval C_arb(const int N, const int n, const int l, const int n1, const int l1, const int k, const fpval* X_tab)
{
	fpval result=0;
	for(int j = 0; j <= min(n-l-1, k); ++j)
	{
		for(int j1 = 0; j1 <= min(n1-l1-1, k); ++j1)
		{
			//printf("n %d l %d n1 %d l1 %d k %d   j %d j1 %d\n", n, l, n1, l1, k,  j, j1);
			if(j + j1 == k)
				result += X_tab[((n - 1)*N + l)*N + j] * X_tab[((n1 - 1)*N + l1)*N + j1];//*X[((n1*NMAX+l1)*2*NMAX+j1)];
		}
	}
	return result;
}

fpval wigner_3j_symbol_arb(const int j1, const int j2, const int j3, const int m1, const int m2, const int m3, const fpval* desc)
{
//======================================================================
// After Wigner3j.m by David Terr, Raytheon, 6-17-04
//
// Compute the Wigner 3j symbol using the Racah formula.
//
//  / j1 j2 j3 \    '
//  |          |    '
//  \ m1 m2 m3 /    '
//
// Reference: Wigner 3j-Symbol entry of Eric Weinstein's Mathworld:
// http://mathworld.wolfram.com/Wigner3j-Symbol.html
//======================================================================

    // Error checking
    if ( ( 2*j1 != floor(2*j1) ) || ( 2*j2 != floor(2*j2) ) || ( 2*j3 != floor(2*j3) ) || ( 2*m1 != floor(2*m1) ) || ( 2*m2 != floor(2*m2) ) || ( 2*m3 != floor(2*m3) ) )
    {
        printf("All arguments must be integers or half-integers.\n");
        exit(0);
    }

    // Additional check if the sum of the second row equals zero
    if ( m1+m2+m3 != 0 )
    {
    	printf("3j-Symbol unphysical\n");
    }

    if ( j1 - m1 != floor ( j1 - m1 ) )
    {
    	printf("2*j1 and 2*m1 must have the same parity\n");
    }

    if ( j2 - m2 != floor ( j2 - m2 ) )
    {
    	printf("2*j2 and 2*m2 must have the same parity\n");
    }

    if ( j3 - m3 != floor ( j3 - m3 ) )
    {
    	printf("2*j3 and 2*m3 must have the same parity\n");
    }

    if ( (j3 > j1 + j2)  || ( j3 < abs(j1 - j2)) )
    {
    	printf("j3 is out of bounds.\n");
    }

    if (abs(m1) > j1)
    {
    	printf("m1 is out of bounds.\n");
    }

    if (abs(m2) > j2)
    {
    	printf("m2 is out of bounds.\n");
    }

    if (abs(m3) > j3)
    {
    	printf("m3 is out of bounds.\n");
    }

    int t1 = j2 - m1 - j3;
    int t2 = j1 + m2 - j3;
    int t3 = j1 + j2 - j3;
    int t4 = j1 - m1;
    int t5 = j2 + m2;

    int tmin = max( 0, max( t1, t2 ) );
    int tmax = min( t3, min( t4, t5 ) );

	fpval wigner = 0;
	fpval one;
    for(int t = tmin; t <= tmax; ++t)
    {
    	if((t)%2 == 0)
    	{
    		one = 1;
    	}
    	else
    	{
    		one = -1;
    	}
        /*
    	wigner += one / ( descend_factorial1(desc, t) *\
    						descend_factorial1(desc, t-t1) *\
    						descend_factorial1(desc, t-t2) *\
    						descend_factorial1(desc, t3-t) *\
    						descend_factorial1(desc,t4-t) *\
    						descend_factorial1(desc,t5-t) );
        */
    	wigner += one / ( get_desc_fact_arb(desc, t) *\
    						get_desc_fact_arb(desc, t-t1) *\
    						get_desc_fact_arb(desc, t-t2) *\
    						get_desc_fact_arb(desc, t3-t) *\
    						get_desc_fact_arb(desc,t4-t) *\
    						get_desc_fact_arb(desc,t5-t) );
    }
	if((j1 - j2 - m3)%2==0)
	{
		one = 1;
	}
	else
	{
		one = -1;
	}

    wigner *= one * sqrt( get_desc_fact_arb(desc, j1+j2-j3) * get_desc_fact_arb(desc, j1-j2+j3) * get_desc_fact_arb(desc, -j1+j2+j3) /\
    						get_desc_fact_arb(desc, j1+j2+j3+1) * get_desc_fact_arb(desc, j1+m1) * get_desc_fact_arb(desc, j1-m1) *\
    						get_desc_fact_arb(desc, j2+m2)      * get_desc_fact_arb(desc, j2-m2) * get_desc_fact_arb(desc, j3+m3) *\
    						get_desc_fact_arb(desc,j3-m3)\
    		            );

	return wigner;
}

struct A_array_arb* generate_A_array_arb(const int N, const fpval* desc_fact)
{
	struct A_array_arb* Ak;
	Ak = allocate_A_array_arb(N);
	for(int l = 0; l < N; ++l)
	{
		printf("\rl = %d ", l);
		fflush(stdout);
		for(int l1 = 0; l1 < N; ++l1)
		{
			for(int m = 0; m <= min(l, l1); ++m)
			{
				for(int k = abs(l - l1); k <= l + l1; ++k)
				{
					fpval rootInsides = (2*l + 1) * (2*l1 + 1);
					fpval one;
					if(((k + l1 - l)/2 + m)%2==0)
					{
						one = 1;
					}
					else
					{
						one = -1;
					}

					fpval val = one*(2*k + 1)*sqrt(rootInsides) *\
									wigner_3j_symbol_arb(l, l1, k, 0, 0, 0, desc_fact)*wigner_3j_symbol_arb(l, l1, k, m, -m, 0, desc_fact);
					Ak->data[index_A_array(N, l, l1, m, k)] = val;

                    /*
					fpval val=one*(2*k+1)*sqrt(rootInsides) *\
									j_symb_type_3(l, l1, k, 0, 0, 0, desc_fact)*j_symb_type_3(l, l1, k, m, -m, 0, desc_fact);
					Ak->data[index_A_array(N, l, l1, m, k)] = val;

					val =j_symb_type_3(l, l1, k, m, -m, 0, desc, rise);
					printf("l %d l1 %d m %d k %d val=%.30f\n", l, l1, m, k, to_d(zindex_matrix_4d(M, l, l1, m, k)));
					gmp_printf("l %d l1 %d m %d k %d Ak %.30Ff\n", l, l1, m, k, val.get_mpf_t());
                    */
				}
			}
		}
	}
	printf("\n");

	return Ak;
}

fpval laguerre_arb(const fpval x, const fpval upper, const int lower)
{
	//int i; //Index.
	fpval i; //Index.
	fpval one =1.0;
	fpval two =2.0;

	fpval current_L = -x + upper + 1.0000;
	fpval prev_L = 1.00;
	fpval dp_L; //double previous L.

	if (lower == 0) {return prev_L;}

	for (i = 2;i <= lower; ++i)//The algorithm starts at L_0^upper (= 1) and uses the recursion relation until L_lower^upper
	{
		dp_L = prev_L;
		prev_L = current_L;
		current_L = one/i * ( (two*i - one + upper - x)*prev_L - (i - one + upper)*dp_L );//increment lower by 1.
	}

	return current_L;
}

fpval inv_Bessel_integr_arb(const int N, const int n, const int l, const int n1, const int l1, const int k, const fpval rho2, const fpval* desc, const fpval* X_tab)
{
	fpval result = 0;
	fpval alpha = k;
	alpha  += 0.5;
	if(n - l + n1 - l1 - 2 < 2)
	{
		for(int j = 0; j <= n - l + n1 - l1 - 2; j++)
		{
			//alpha   = k;
			//alpha  += 0.5;
			result += C_arb(N, n, l, n1, l1, j, X_tab)*get_desc_fact_arb(desc, j+(l+l1-k)/2)*laguerre_arb(rho2/4.0, alpha, j+(l+l1-k)/2);
		}
	}
	else
	{
		int coeff0 = (l+l1-k)/2;
		fpval Lprev = laguerre_arb(rho2/4.0, alpha, 0 + coeff0);
		result += C_arb(N, n, l, n1, l1, 0, X_tab)*get_desc_fact_arb(desc, 0+coeff0)*Lprev;
		fpval Lcur  = laguerre_arb(rho2/4.0, alpha, 1 + coeff0);
		result += C_arb(N, n, l, n1, l1, 1, X_tab)*get_desc_fact_arb(desc, 1+coeff0)*Lcur;
		fpval Lnext = 0;
		for(int j = 2; j <= n - l + n1 - l1 - 2; j++)
		{
			Lnext   = (2.0*(-1 + j + coeff0) + alpha + 1.0 - rho2/4.0)*Lcur - (-1 + j + coeff0 + alpha)*Lprev;
			Lnext  /= (j + coeff0);
			result += C_arb(N, n, l, n1, l1, j, X_tab)*get_desc_fact_arb(desc, j + coeff0)*Lnext;
			Lprev   = Lcur;
			Lcur    = Lnext;
		}

	}
	//fpval expon = fpval(0.363309569359011254550541411658);
	return result*sqrt(pow(rho2/4.0, k))*exp(to_d(-rho2/4.0));
}

struct T_array_arb* generate_T_array_arb(const int N, const double z)
{
    mpf_set_default_prec(GMP_PREC);
	struct T_array_arb* T_arb;
	T_arb = allocate_T_array_arb(N);
    fpval *descFact;
    fpval *riseFact;
    fpval *X_tab;
    fpval half = 0.5;

    descFact = generate_desc_fact_array_arb(4*N);
    riseFact = generate_rise_fact_array_arb(half, 4*N);
    X_tab = generate_X_array_arb(N, descFact, riseFact);

    printf("Calculating Ak elements...\n");
    struct A_array_arb* A;
    A = generate_A_array_arb(N, descFact);
    printf("done.\n");

	fpval R = z;
	int sign = (R<0)?(-1):(1);
	fpval rho2 = R*R;
	rho2  /= 20.0;
	fpval integral;
	fpval val;

	printf("Calculating T elements:\n");
	for(int n = 1; n <= N; ++n)
	{
		printf("\rn = %d ", n);
		fflush(stdout);
		for(int l = 0; l < n; ++l)
		{
			for(int n1 = 1; n1 <= n; ++n1)
			{
				for(int l1 = 0; l1 < n1; ++l1)
				{
					for(int k = abs(l-l1); k <= l + l1; k += 2)
					{
						integral = inv_Bessel_integr_arb(N, n, l, n1, l1, k, rho2, descFact, X_tab);
						for(int m = 0; m <= min(l, l1); ++m)
						{
							val = pow((double)sign, (double)(l1-l))*A->data[index_A_array(N, l, l1, m, k)]*integral;
							T_arb->data[index_T_array(N, n, l, n1, l1, m)] += val;
						}
					}
				}
			}
		}
	}
	for(int n = 1; n <= N; ++n)
	{
		for(int l = 0; l < n; ++l)
		{
			for(int n1 = 1; n1 <= n; ++n1)
			{
				for(int l1 = 0; l1 < n1; ++l1)
				{
					for(int m = 0; m <= min(l, l1); ++m)
					{
						val  = T_arb->data[index_T_array(N, n, l, n1, l1, m)];
						val *= pow(-1.0, (double)(l1-l));
						T_arb->data[index_T_array(N, n1, l1, n, l, m)] = val;
					}
				}
			}
		}
	}
	printf("\rdone.  \n");

	delete[] descFact;
	delete[] riseFact;
	delete[] X_tab;
	deallocate_A_array_arb(A);
	return T_arb;
}

//
//	Double precision functions and stuff
//
struct T_array* allocate_T_array(const int N)
{
	struct T_array* T = (struct T_array*) calloc (1, sizeof(struct T_array));
	T->N = N;
	T->data = (double*)calloc(N*N*N*N*N, sizeof(double));
	return T;
}

void deallocate_T_array(struct T_array* T)
{
	free(T->data);
	free(T);
}

struct spf_coeffs* translate_spf_coeffs(const struct spf_coeffs* coeffs, const struct T_array* T)
{
	int N = coeffs->N;
	struct spf_coeffs* tcoeffs;
	tcoeffs = allocate_spf_coeffs(N);
	for(int n = 1; n <= N; ++n)
	{
		for(int l = 0; l < n; ++l)
		{
			for(int m = -l; m <= l; ++m)
			{
				double acc_r = 0.0;
				double acc_i = 0.0;
				for(int k = 1; k <= N; ++k)
				{
					for(int j = 0; j < k; ++j)
					{
						if(abs(m) <= j)
						{
							acc_r += T->data[index_T_array(N, n, l, k, j, abs(m))] * coeffs->re[index_spf_coeffs(N, k, j, m)];
							acc_i += T->data[index_T_array(N, n, l, k, j, abs(m))] * coeffs->im[index_spf_coeffs(N, k, j, m)];
						}
					}
				}
				tcoeffs->re[index_spf_coeffs(N, n, l, m)] = acc_r;
				tcoeffs->im[index_spf_coeffs(N, n, l, m)] = acc_i;
			}
		}
	}

	return tcoeffs;
}

//
//	Combined stuff
//
void tab_T_array(const int N, const double z, const char* path)
{
	struct T_array_arb* T_arb;
	T_arb = generate_T_array_arb(N, z);

    char T_file[512];
    sprintf(T_file, "%s/%s/%s_%d_%.2lf.dat", path, TFOLDER, TFILE, NMAX, z);
    printf("Writing translation matrix to file %s\n", T_file);
	FILE *f = fopen(T_file, "wb");
	if(f == NULL)
	{
    	fprintf (stderr, "File %s could not be opened.\n", T_file);
        exit (EXIT_FAILURE);
	}
	double val;
	for (int n = 1; n <= N; ++n)
	{
		for (int n1 = 1; n1 <= n; ++n1)////////////////////////
		{										///////I switched these two to compare with existing
			for (int l = 0; l < n/*N*/; ++l)	///////
			{
				for (int l1 = 0; l1 < n1; ++l1)
				{
					int m_max = (l < l1 ? l : l1);
					for (int m = 0; m <= m_max; ++m)
					{
						val = to_d(T_arb->data[index_T_array(N, n, l, n1, l1, m)]);
						fwrite(&val, sizeof(double), 1, f);
					}
				}
			}
		}
	}
	fclose(f);
	deallocate_T_array_arb(T_arb);
}

void load_T_array(struct T_array* T, const double z, const char* path)
{
	// Binary input file
	int N = T->N;
	double val;
	int err;

    char T_file[512];
    sprintf(T_file, "%s/%s/%s_%d_%.2lf.dat", path, TFOLDER, TFILE, NMAX, z);
	FILE *f = fopen(T_file, "rb");
	if(f == NULL)
	{
    	fprintf (stderr, "File %s could not be opened.\n", path);
        exit (EXIT_FAILURE);
	}

	for (int n = 1; n <= N; ++n)
	{
		for (int n1 = 1; n1 <= n; ++n1)////////////////////////
		{										///////I switched these two to compare with existing
			for (int l = 0; l < n/*N*/; ++l)	///////
			{
				for (int l1 = 0; l1 < n1; ++l1)
				{
					int m_max = (l < l1 ? l : l1);
					for (int m = 0; m <= m_max; ++m)
					{
						err = fread(&val, sizeof(double), 1, f);
						if(err != 1)
						{
							fprintf(stderr, "Couldn't read matrix element from binfile:\n");
							fprintf(stderr, "n = %d, n1 = %d, l = %d, l1 = %d, m = %d, exiting...\n", n, n1, l, l1, m);
							exit (EXIT_FAILURE);
						}
						T->data[index_T_array(N, n, l, n1, l1, m)] = val;
						T->data[index_T_array(N, n1, l1, n, l, m)] = val*pow(-1.0, (double)(l1-l));
					}
				}
			}
		}
	}
	fclose(f);
}

void get_T_array(struct T_array* T, const double z, const char *path)
{
    char T_file[512];
    sprintf(T_file, "%s/%s/%s_%d_%.2lf.dat", path, TFOLDER, TFILE, NMAX, z);
    if (access(T_file, F_OK) == -1)
    {
        //tab_T_array(NMAX, z, path);
    	fprintf(stderr, "Couldn't find the appropriate translation matrix file: %s\n", T_file);
    	fprintf(stderr, "You can generate translation matrix files using the gen_trans_matrices.sh script\n");
    	fprintf(stderr, "Exiting\n");
    	exit(EXIT_FAILURE);
    }

//    printf ("Loading T\n");
    load_T_array(T, z, path);
}
////////////////////////////////////////////////////////////////////
// Some utilites That might not be needed, but are used at present.

//TODO Decide if needed
fpval get_desc_fact_arb(const fpval* desc, const int n)
{
	return desc[n];
}

//TODO Decide if needed
//fpval get_rise_fact_arb(const fpval* rise, const fpval x, const int n)
//{
//	return rise[n];
//}

//TODO Decide if needed
fpval descend_factorial_arb(const int n)
{
	fpval result = 1;
	for(fpval i = 1; i <= n; ++i)
	{
		result *= i;
	}
	return result;
}

//TODO Decide if needed
fpval rising_factorial_arb(const fpval x, const int n)
{
	fpval result = x;
	for(fpval i = 1; i < n; ++i)
	{
		result *= (x+i);
	}
	return result;
}


///////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// Should get rid of this
