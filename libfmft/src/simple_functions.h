/******************************************************************************
Copyright (c) 2015 Dzmitry Padhorny, Andrey Kazennov and Dima Kozakov

Permission is hereby granted, free of charge, to any person obtaining a copy 
of this software and associated documentation files (the "Software"), to deal 
in the Software without restriction, including without limitation the rights 
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
SOFTWARE.
*******************************************************************************/
#pragma once
#include <errno.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>

////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
// The important functions.
/**
 * Calculates the phi angle of spherical coordinate system
 * from position in Cartesian coordinate system.
 * \param x Position along the x axis of Cartesian coordinate system.
 * \param y Position along the y axis of Cartesian coordinate system.
 */
float determine_phi_over_2PI(float x,float y);

/**
 * Calculates the theta angle of spherical coordinate system
 * from position in Cartesian coordinate system.
 * \param z Position along the z axis of Cartesian coordinate system.
 * \param r Distance from the center of Cartesian coordinate system.
 */
float determine_theta(float z, float r);

#ifndef DOXYGEN_SHOULD_SKIP_THIS
////////////////////////////////////////////////////////////////////
// Some utilites That might not be needed, but are used at present.
void my_printf(const int mpi_rank, const char* fmt, ... );

////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
// Should get rid of this

char** file2array_of_strings(char file_name[80], int* array_length);
int mygetline(char** string,int* string_length, FILE* stream);
int file2array(char file_name[80], double **array);
int file2complex_array(char file_name[80], double **array_re, double **array_im);
void fprint_array(char* file_name,double* array,int size);
void fprint_complex_array(char* file_name,double* re, double* im, int size);
void fprint_float_array(char* file_name,float* array,int size);
void fprint_square_matrix(char* file_name,double* array,int legnth);

#endif // DOXYGEN_SHOULD_SKIP_THIS
