/******************************************************************************
Copyright (c) 2015 Dzmitry Padhorny, Andrey Kazennov and Dima Kozakov

Permission is hereby granted, free of charge, to any person obtaining a copy 
of this software and associated documentation files (the "Software"), to deal 
in the Software without restriction, including without limitation the rights 
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
SOFTWARE.
*******************************************************************************/
//These next macros are to standardize and streamline the code.

//The index is for a spherical harmonic polynomials.
#define PLM_INDEX(l, m) ((l)*((l)+1)/2 + (m))

//Index for spherical harmonic coefficient.
#define LM_INDEX(l, m) ((l)*((l)+1) + (m))
#define NEW_LM_INDEX(N, l, m) (N * (m + N-1) + l)

//The index is for a laguerre transformed spherical harmonic coefficient
#define NLM_INDEX(n, l, m) ((((n)-1)*((n)-1)*((n)-1)*2 + ((n)-1)*((n)-1)*3 + ((n)-1))/6 + LM_INDEX((l),(m)))
#define NEW_NLM_INDEX(N, n, l, m) (((N) * (m + (N) - 1) + (l)) * (N) + (n) - 1)
#define NEW2_NLM_INDEX(N, n, l, m) 2 * (((N) * (m + (N) - 1) + (l)) * (N) + (n) - 1)
#define NEW3_NLM_INDEX(N, n, l, m) ((2*N - 1) * ((N) * ((n)-1) + (l)) + m + N-1)

//The index is for the surficial and laguerre where the lower index and upper index are "n" and "l", respectively
#define NL_INDEX(lower, upper) (((lower)-1)*(lower)/2 + (upper))
#define NEW_NL_INDEX(N, lower, upper) (N * (upper) + (lower-1))

//This index is for the Laguerre arrays with lower index "n-l-1" and upper index "l+1/2"
#define NL_INDEX_FOR_LAGUERRE(lower,upper,bw) ((lower)*(bw) - (lower)*((lower)-1)/2 + (upper))

//This index is for making a full, square, matrix
#define SQUARE_INDEX(lower,upper,bw) ((lower)*(bw) + (upper))


#define BBIJK_INDEX(N, b, b1, i, j, k)     (((((b) * (N)) + (b1)) * (2*(N) - 1) + (i)) * (2 * (N) - 1) + (j)) * (2 * (N) - 1) + (k)
#define IJK_INDEX(N, i, j, k)     ((i)*(2*(N) - 1) + (j)) * (2*(N) - 1) + (k)
#define BBIJK_INDEX_C2R(N, b, b1, i, j, k) (((((b) * (N)) + (b1)) * (2*(N) - 1) + (i)) * (2 * (N) - 1) + (j)) * (N) + (k)
#define IJK_INDEX_C2R(N, i, j, k) ((i)*(2*(N) - 1) + (j)) * (N) + (k)

//indices for 4 main loops
#define LOOP1_INDEX(N, t, m, j, l, i)     ((N) * ((N) * ((N)       * ((N)       * ((N)-1+(t)) + (m))         + (j))       + (l)) + (i)-1)
#define LOOP2_INDEX(N, s, t, m, j, l)     ((N) * ((N) * ((N)       * ((2*(N)-1) * ((N)-1+(s)) + ((N)-1+(t))) + (m))       + (j)) + (l))
#define LOOP3_INDEX(N, beta1, s, t, m, j) ((N) * ((N) * ((2*(N)-1) * ((2*(N)-1) * (beta1)     + ((N)-1+s))   + ((N)-1+t)) + (m)) + (j))
