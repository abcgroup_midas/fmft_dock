/******************************************************************************
Copyright (c) 2015 Dzmitry Padhorny, Andrey Kazennov and Dima Kozakov

Permission is hereby granted, free of charge, to any person obtaining a copy 
of this software and associated documentation files (the "Software"), to deal 
in the Software without restriction, including without limitation the rights 
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
SOFTWARE.
*******************************************************************************/
#pragma once
/**
 * This functions checks whether the specified rotation matrices
 * correspond to "close" rotations.
 * By "close" we mean that that there exists a rotation matrix C,
 * for which B = AC, and C corresponds to a rotation about a certain
 * axis by angle not greater then delta_theta
 *\param A Rotation matrix A (Z-Y-Z rotation)
 *\param B Rotation matrix B (Z-Y-Z rotation)
 *\param delta_theta Threshold;
 *\return 1 if rotations are "close" and 0 otherwise
 */
int compare_rot_mats(double A[3][3], double B[3][3], double delta_theta);
double rot_angle_from_rot_mat(double A[3][3]);
double trace_rot_mat(double A[3][3]);
void transpose_rot_mat(double AT[3][3], double A[3][3]);
void mult_rot_mats(double C[3][3], double A[3][3], double B[3][3]);
////////////////////////////////////////////////////////////////////
// Some utilites That might not be needed, but are used at present.

/**
 * This functions checks whether the specified rotation matrices
 * correspond to "close" rotations. This is a faster implementation of
 * compare_rot_mats() routine.
 * By "close" we mean that that there exists a rotation matrix C,
 * for which B = AC, and C corresponds to a rotation about a certain
 * axis by angle not greater then delta_theta
 *\param A Rotation matrix A (Z-Y-Z rotation)
 *\param B Rotation matrix B (Z-Y-Z rotation)
 *\param c_delta_theta Cosine of the threshold angle delta_theta;
 *\return 1 if rotations are "close" and 0 otherwise
 */
int compare_rot_mats_fast(double A[3][3], double B[3][3], double c_delta_theta);

////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
// Should get rid of this

