/******************************************************************************
Copyright (c) 2015 Dzmitry Padhorny, Andrey Kazennov and Dima Kozakov

Permission is hereby granted, free of charge, to any person obtaining a copy 
of this software and associated documentation files (the "Software"), to deal 
in the Software without restriction, including without limitation the rights 
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
SOFTWARE.
*******************************************************************************/
#include <math.h>
#include "angle_distance.h"
#include "rotational_matrix.h"
#include <stdlib.h>

int compare_rot_mats(double A[3][3], double B[3][3], double delta_theta)
{
	double AT[3][3];
	transpose_rot_mat(AT, A);

	double C[3][3];
	mult_rot_mats(C, AT, B);

	double theta = rot_angle_from_rot_mat(C);
	//printf("theta: %f, theta ang: %f\n", theta, theta*180.0/M_PI);

	if (theta < delta_theta)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

double rot_angle_from_rot_mat(double A[3][3])
{
	double tr = (trace_rot_mat(A) - 1.0)/2.0;
	if (tr < -1.0)
	{
		tr=-1.0;
	}
	else if (tr > 1.0)
	{
		tr=1.0;
	}
	double theta = acos(tr);

	return theta;
}

double trace_rot_mat(double A[3][3])
{
	double tr = A[0][0] + A[1][1] + A[2][2];

	return tr;
}

void transpose_rot_mat(double AT[3][3], double A[3][3])
{
	//swap rows and columns
	AT[0][0] = A[0][0];
	AT[1][0] = A[0][1];
	AT[2][0] = A[0][2];

	AT[0][1] = A[1][0];
	AT[1][1] = A[1][1];
	AT[2][1] = A[1][2];

	AT[0][2] = A[2][0];
	AT[1][2] = A[2][1];
	AT[2][2] = A[2][2];

}

void mult_rot_mats(double C[3][3], double A[3][3], double B[3][3])
{
  //double [3][3] C = (double [3][3])_mol_malloc(sizeof(struct rmatrix));

  C[0][0] = A[0][0]*B[0][0] + A[0][1]*B[1][0] + A[0][2]*B[2][0];
  C[0][1] = A[0][0]*B[0][1] + A[0][1]*B[1][1] + A[0][2]*B[2][1];
  C[0][2] = A[0][0]*B[0][2] + A[0][1]*B[1][2] + A[0][2]*B[2][2];

  C[1][0] = A[1][0]*B[0][0] + A[1][1]*B[1][0] + A[1][2]*B[2][0];
  C[1][1] = A[1][0]*B[0][1] + A[1][1]*B[1][1] + A[1][2]*B[2][1];
  C[1][2] = A[1][0]*B[0][2] + A[1][1]*B[1][2] + A[1][2]*B[2][2];

  C[2][0] = A[2][0]*B[0][0] + A[2][1]*B[1][0] + A[2][2]*B[2][0];
  C[2][1] = A[2][0]*B[0][1] + A[2][1]*B[1][1] + A[2][2]*B[2][1];
  C[2][2] = A[2][0]*B[0][2] + A[2][1]*B[1][2] + A[2][2]*B[2][2];

}

////////////////////////////////////////////////////////////////////
// Some utilites That might not be needed, but are used at present.
int compare_rot_mats_fast(double A[3][3], double B[3][3], double c_delta_theta)
{
	double C00, C11, C22;
	//C=AT*B
	C00 = A[0][0]*B[0][0] + A[1][0]*B[1][0] + A[2][0]*B[2][0];
	C11 = A[0][1]*B[0][1] + A[1][1]*B[1][1] + A[2][1]*B[2][1];
	C22 = A[0][2]*B[0][2] + A[1][2]*B[1][2] + A[2][2]*B[2][2];

	double tr = (C00 + C11 + C22 - 1.0)/2.0;

	if(tr > c_delta_theta)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
// Should get rid of this

