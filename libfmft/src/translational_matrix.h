/******************************************************************************
Copyright (c) 2015 Dzmitry Padhorny, Andrey Kazennov and Dima Kozakov

Permission is hereby granted, free of charge, to any person obtaining a copy 
of this software and associated documentation files (the "Software"), to deal 
in the Software without restriction, including without limitation the rights 
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
SOFTWARE.
*******************************************************************************/
/** \file translational_matrix.h
	This file contains structures and functions
	that are used for translation in SPF space.
	A part of this functions uses GMP library (http://gmplib.org/)
	for high precision calculations.
*/
#pragma once

#include <string.h>
#include <gmpxx.h>
#include "index.h"
#include "spf_coefficients.h"
#define TFOLDER "data"
#define TFILE "transMatrix"
#define NMAX 32 /* Polinomial order, for which translation matrices will be tabulated.*/
#define GMP_PREC 256 /* Precision in bits, for which elements of translation matrix will be calculated.*/

////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
// The important functions.
typedef mpf_class fpval;


/**
 * Indexer for
 * arrays storing elements of translation matrix.
 * \param N Highest polynomial order, for which the matrix elements are stored.
 * \param n In range [1, N].
 * \param l In range [0, n-1].
 * \param n1 In range [1, N].
 * \param l1 In range [0, n-1].
 * \param m In range [0, min(l, l1)]
 */
inline int index_T_array(int N, int n, int l, int n1, int l1, int m)
{
	return N*(N*(N*(N*m + l) + l1) + n-1) + n1-1;
}


/**
 * Indexer for
 * arrays storing elements of A matrix.
 * \param N Highest polynomial order, for which the matrix elements are stored.
 * \param l In range [0, N-1].
 * \param l1 In range [0, N-1].
 * \param m In range [0, min(l, l1)]
 * \param k In range [abs(l-l1), l+l1]
 */
inline int index_A_array(int N, int l, int l1, int m, int k)
{
	return 2*N*(N*(N*l + l1) + m) + k;
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int min(int i, int j);
int max(int i, int j);
#endif // DOXYGEN_SHOULD_SKIP_THIS
//
//	Arbitrary precision functions and stuff
//

#ifndef DOXYGEN_SHOULD_SKIP_THIS
mpf_class pow(const mpf_class &a, const int n);


inline double to_d(const mpf_class &a)
{
	return a.get_d();
}


inline double to_d(const double a)
{
	return a;
}
#endif // DOXYGEN_SHOULD_SKIP_THIS


/**
 * This struct is used for storing elements of translation matrix
 * calculated up to polynomial order N in precision defined by the GMP_PREC constant.
 */
struct T_array_arb
{
	int N; /**< Highest polinoial order, for which matrix elements are stored.*/
	fpval* data; /**< Array of size N*N*N*N*N. */
};

/**
 * This struct is used for storing elements of translation matrix
 * calculated up to polynomial order N in precision defined by the GMP_PREC constant.
 */
struct A_array_arb
{
	int N; /**< Highest polinoial order, for which matrix elements are stored.*/
	fpval* data; /**< Array of size N*N*N*N. */
};


/**
 * Allocates an instance of T_array_arb struct
 * that is intended to store translation matrix elements
 * up to polynomial order N in precision defined by the GMP_PREC constant.
 * \param N Highest polynomial order, for which matrix elements will be stored.
 * \return Pointer to the allocated struct.
 */
struct T_array_arb* allocate_T_array_arb(const int N);


/**
 * Deallocates an instance of T_array_arb struct.
 */
void deallocate_T_array_arb(struct T_array_arb* T_arb);


/**
 * Allocates an instance of A_array_arb struct
 * that is intended to store A matrix elements
 * up to polynomial order N in precision defined by the GMP_PREC constant.
 * \param N Highest polynomial order, for which matrix elements will be stored.
 * \return Pointer to the allocated struct.
 */
struct A_array_arb* allocate_A_array_arb(const int N);

/**
 * Deallocates an instance of A_array_arb struct.
 */
void deallocate_A_array_arb(struct A_array_arb* A_arb);


/**
 * Allocates an array of arbitrary precision floating point elements
 * of size n+1
 * and fills it with values of descending (i.e. normal) factorial
 * of each i in range [0, n].
 * \param n Highest number, for which the factorial will be calculated.
 * \return Pointer to the filled array.
 */
fpval* generate_desc_fact_array_arb(const int n);


/**
 * Allocates an array of arbitrary precision floating point elements
 * of size n+1
 * and fills it with values of rising factorial of x
 * for each i in range [0, n].
 * Rising factorial x_i = x*(x+1)...(x+i-1)
 * \param x Value, for each rising factorials will be caclulated.
 * \param n Highest number, for which the factorial will be calculated.
 * \return Pointer to the filled array.
 */
fpval* generate_rise_fact_array_arb(const fpval x, const int n);


fpval X_arb(const int n, const int l, const int j, const fpval* desc_fact, const fpval* rise_fact_half);


/**
 * Allocates an array of arbitrary precision floating point elements
 * of size N*N*N
 * and fills it with values returned by X_arb()
 * for each n in range [1, N], l in range [0, n-1], j in range [0, n-l-1].
 * \param N Highest polynomial order, for which X_arb will be calculated.
 * \param desc_fact Pointer to an array of descending factorials generated with generate_desc_fact_array_arb(4*N)
 * \param rise_fact Pointer to an array of rising factorials generated by generate_rise_fact_array_arb(0.5, 4*N)
 * \return Pointer to the filled array.
 */
fpval* generate_X_array_arb(const int N, const fpval* desc_fact, const fpval* rise_fact_half);

/**
 * Calculates wigner 3-j symbol
 * \param desc Pointer to an array of descending factorials generated by generate_desc_fact_array_arb()
 */
fpval wigner_3j_symbol_arb(const int j1, const int j2, const int j3, const int m1, const int m2, const int m3, const fpval* desc);


/**
 * Allocates an instance of A_array arb struct
 * and fills it.
 * \param N Highest polynomial order, for which elements of A matrix will be calculated.
 * \param desc_fact Pointer to an array of descending factorials generated with generate_desc_fact_array_arb()
 * \return Pointer to the filled struct.
 */
struct A_array_arb* generate_A_array_arb(const int N, const fpval* desc_fact);

/**
 * Calculates a value of Laguerre polynomial for parameter x
 * \param x Laguerre polynomial parameter
 * \param upper Laguerre polynomial upper index
 * \param lower Laguerre polynomial lower index
 * \return Value of Laguerre polynomial caculated in GMP_PREC precision.
 */
fpval laguerre_arb(const fpval x, const fpval upper, const int lower);

/**
 * Calculates the integral in eq. (10) of "High-order analytic translation matrix elements for real-space six-dimensional polar Fourier correlations"
 * \param desc_fact Pointer to an array of descending factorials generated with generate_desc_fact_array_arb()
 * \param X_tab Pointer to an array of X entities generated with generate_X_array_arb();
 * \return Value of the integral caculated in GMP_PREC precision.
 */
fpval inv_Bessel_integr_arb(const int N, const int n, const int l, const int n1, const int l1, const int k, const fpval rho2, const fpval* desc, const fpval* X_tab);


/**
 * Allocates an instance of T_array_arb struct and
 * fills it with translation matrix elements calculated for a
 * specific value of translation distance z up to polynomial order N
 * in GMP_PREC bit precision.
 * \return Pointer to the filled struct.
 */
struct T_array_arb* generate_T_array_arb(const int N, const double z);

//
//	Double precision functions and stuff
//
/**
 * This struct is used for storing elements of translation matrix
 * calculated up to polynomial order N in double precision.
 */
struct T_array
{
	int N;
	double* data;
};

/**
 * Allocates an instance of A_array struct
 * that is intended to store A matrix elements
 * up to polynomial order N in double precision..
 * \param N Highest polynomial order, for which matrix elements will be stored.
 * \return Pointer to the allocated struct.
 */
struct T_array* allocate_T_array(const int N);


/**
 * Deallocates an instance of A_array struct.
 */
void deallocate_T_array(struct T_array* T);


/**
 * Allocates an instance of spf_coeffs struct and
 * fills it with the result of multiplication of a set of SPF
 * coefficients stored by coeffs spf_coeffs struct by a translation matrix T
 * (Which performs translation of the property represented by the coeffs set of SPF coefficients
 * along z axis by distance z for which the translation matrix was calculated).
 * Note that the highest polynomial order of a set of SPF coefficients N is supposed to be the same as
 * the highest polynomial order of translation matrix.
 * \param coeffs Pointer to an instance of spf_coeffs struct, storing a set of SPF coefficients to be rotated.
 * \param T Pointer to an instance of T_array struct, storing elements translation matrix.
 * \return Pointer to the instance of spf_coeffs struct, filled with "rotated" coefficients.
 */
struct spf_coeffs* translate_spf_coeffs(const struct spf_coeffs* coeffs, const struct T_array* T);

//
//	Combined stuff
//
/**
 * Generates elements of translation matrix in GMP_PREC precision and writes
 * them in double precision into a file named (TFILE)_(N)_(z).dat in a folder determined
 * by the path parameter (z is taken with 2 decimal values after the dot).
 * Filename TFILE is a constant defined in translational_matrix.h.
 * Note that file written for a matrix calculated up to order N
 * can be used to fill matrices for all orders not greater than N,
 * so that's a good idea to tabulate the matrices calculated up to the highest
 * order you might further need and reuse these files.
 * \param N Highest polynomial order, for which matrix elements will be calculated and stored.
 * \param z Translation distance, for which the matrix will be caclulated.
 * \param path Path to the folder, in which (TFILE)_(N)_(z).dat file will be created.
 */
void tab_T_array(const int N, const double z, const char* path);


/**
 * Allocates an instance of T_array struct and
 * fills it according to a file named (TFILE)_(NMAX)_(z).dat located in
 * a folder determined by the path parameter.
 * Filename TFILE is a constant defined in translational_matrix.h.
 * Polynomial order NMAX is a constant defined in translational_matrix.h.
 * z is translation distance taken with 2 decimal values after the dot.
 * The file is supposed to contain translation matrix elements calculated
 * for translation distance z up to polynomial order N.
 * Note, that you can use tabulated matrices caclulated for higher order
 * to initialize matrices for any order not greater than that of the tabulated matrix.
 * \param T Pointer to a pre-allocated T_array struct into which matrix elements will be read.
 * \param z Translation distance.
 * \param path Path to the folder, in which (DFILE_NEW)_(L).dat file is located.
 * \return Pointer to the filled many_d_arrays struct.
 */
void load_T_array(struct T_array* T, const double z, const char* path);


/**
 * This function checks the existence of a file named (TFILE)_(NMAX)_(z).dat
 * in folder determined by the path parameter and, if such file exists,
 * tries to fill a pre-allocated instance of T_array struct
 * using load_T_array() function.
 * If this file is not found, the function creates it using
 * tab_T_array() function and once again tries to allocate
 * and fill an instance of T_array struct
 * using load_T_array() function.
 * \param T Pointer to a pre-allocated instance of T_array struct.
 * \param z TRanslation distance.
 * \param path Path to the folder, in which (DFILE_NEW)_(L).dat file is located (or is intended to be created).
 * \return Pointer to the filled many_d_arrays struct.
 */
void get_T_array(struct T_array* T, const double z, const char *path);


////////////////////////////////////////////////////////////////////
// Some utilites That might not be needed, but are used at present.
#ifndef DOXYGEN_SHOULD_SKIP_THIS

fpval descend_factorial_arb(const int n);


fpval rising_factorial_arb(const fpval x, const int n);



fpval  get_desc_fact_arb(const fpval* desc, const int n);


fpval  get_rise_fact_arb(const fpval* rise, const fpval x, const int n);

#endif // DOXYGEN_SHOULD_SKIP_THIS

///////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// Should get rid of this
