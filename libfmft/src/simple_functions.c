/******************************************************************************
Copyright (c) 2015 Dzmitry Padhorny, Andrey Kazennov and Dima Kozakov

Permission is hereby granted, free of charge, to any person obtaining a copy 
of this software and associated documentation files (the "Software"), to deal 
in the Software without restriction, including without limitation the rights 
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
SOFTWARE.
*******************************************************************************/
#include "simple_functions.h"

#define _MY_ROOT_ 0
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
// The important functions.
//Returns a value between 0 and 1.  This would be the proportion of the circle.
float determine_phi_over_2PI(float x,float y)
{
        const float PI = 3.14159265358979323846;
	float value;
        //printf("x = %f, y=%f, atan(y/x)/2PI = %f\n",x, y, atan(y/x)/(2*PI));
	if (x > 0.00)
        {
		value = atan(y/x)/(2.00*PI);
                if (y >= 0.00)
                {
			return value;
                }
                else
                {
                        return ( 1.00 + value );
                }
        }
        else if (x < 0.00)
        {
		value = atan(y/x)/(2*PI);
                return ( 0.50 + value );
        }
        else
        {
                if (y >= 0.00)
                {
                        return 0.25;
                }
                else
                {
                        return 0.75;
                }
	}
}

//Returns the value between 0 and PI.
float determine_theta(float z, float r)
{
        if (r != 0)
        {
                return acos(z/r);
        }
        else
        {
                return 0;
        }
}

////////////////////////////////////////////////////////////////////
// Some utilites That might not be needed, but are used at present.
void my_printf(const int mpi_rank, const char* fmt, ... )
{
    // do somehthing custom

    va_list args;
    va_start( args, fmt );

#ifdef _MPI_
    if (mpi_rank == _MY_ROOT_)
    {
    	vprintf( fmt, args );
    }
#else
    vprintf( fmt, args );
#endif

    va_end( args );
}

////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
// Should get rid of this

char** file2array_of_strings(char file_name[80], int* array_length)
{
        char** array;
        int string_count=0,size_of_array=10;
        int string_length=0;
        char* temp_string;
        FILE* file_name_ptr;
        if ( (file_name_ptr = fopen(file_name,"r")) == NULL)
        {
                fprintf(stderr, "File %s could not be opened.\n", file_name);
                exit(EXIT_FAILURE);
        }
        array = (char**)malloc(size_of_array*sizeof(char*));
        while (mygetline(&temp_string,&string_length,file_name_ptr) != -1)
        {
                if (string_count>size_of_array)
                {
                        size_of_array *= 2;
                        array =(char**) realloc (array,size_of_array*sizeof(char*));
                }
                array[string_count] = (char*)malloc((string_length+1)*sizeof(char*));
                strcpy(array[string_count],temp_string);
                string_count++;
                free(temp_string);
        }
        fclose(file_name_ptr);
        *array_length = string_count;
        return array;
}

int mygetline(char** string,int* string_length, FILE* stream)
{
        int string_length_guess = 20;
        int single, char_count = 0;;
        *string = (char*) malloc(string_length_guess*sizeof(char));
        single = fgetc(stream);
        if (single == -1)
        {
                return -1;
        }
        while(((char) single != '\n') && (single != -1))
        {
                if (char_count >=  string_length_guess)
                {
                        string_length_guess *= 2;
                        *string = (char*) realloc(*string,string_length_guess*sizeof(char));
                }
                *(*string+char_count) = (char)single;
                single = fgetc(stream);
                char_count++;
        }
        *(*string+char_count) = '\0';
        *string_length = char_count;
        *string = (char*) realloc(*string,(char_count+1)*sizeof(char));
        return char_count;
}



//As the name of this function implies, this is a very simple function that reads in a file of numbers as an array and returns its size.
int file2array(char file_name[80], double **array)
{
        int end_indicator=0, count=0,size_of_array=100;
        FILE *file_name_ptr;
        if ( (file_name_ptr = fopen(file_name,"r")) == NULL)
        {
                fprintf(stderr, "File %s could not be opened.\n", file_name);
                exit(EXIT_FAILURE);
        }
        else
        {
                *array = (double*)malloc(size_of_array*sizeof(double));
                end_indicator = fscanf(file_name_ptr,"%lf", *array);
                while (end_indicator != -1)
                {
                        count++;
                        if (count >= size_of_array)
                        {
                                size_of_array *= 2;
                                *array = (double*) realloc (*array,size_of_array*sizeof(double));
                        }
                        end_indicator = fscanf(file_name_ptr,"%lf", *array+count);
                }
        }
        *array = (double*) realloc (*array,count*sizeof(double));
        fclose(file_name_ptr);
	return count;
}

int file2complex_array(char file_name[80], double **array_re, double **array_im)
{
        int end_indicator=0, count=0,size_of_array=100;
        FILE *file_name_ptr;
        if ( (file_name_ptr = fopen(file_name,"r")) == NULL)
        {
                fprintf(stderr, "File %s could not be opened.\n", file_name);
                exit(EXIT_FAILURE);
        }
        else
        {
                *array_re = (double*)malloc(size_of_array*sizeof(double));
                *array_im = (double*)malloc(size_of_array*sizeof(double));
                end_indicator = fscanf(file_name_ptr,"%lf %lf", *array_re, *array_im);
                while (end_indicator != -1)
                {
                        count++;
                        if (count >= size_of_array)
                        {
                                size_of_array *= 2;
                                *array_re = (double*) realloc (*array_re, size_of_array*sizeof(double));
                                *array_im = (double*) realloc (*array_im, size_of_array*sizeof(double));
                        }
                        end_indicator = fscanf(file_name_ptr,"%lf %lf", *array_re+count, *array_im+count);
                }
        }
        *array_re = (double*) realloc (*array_re, count * sizeof(double));
        *array_im = (double*) realloc (*array_im, count * sizeof(double));
        fclose(file_name_ptr);
	return count;
}

void fprint_array(char* file_name, double* array, int size)
{
	int i;
	FILE* fp;
	fp = fopen( file_name, "w" );
	for (i=0;i<size;i++)
	{
		fprintf(fp,"%.15f\n",array[i]);
	}
	fclose(fp);
	return;
}
void fprint_complex_array(char* file_name,double* re, double* im, int size)
{
	int i;
	FILE* fp;
	fp = fopen( file_name, "w" );
	for (i=0;i<size;i++)
	{
		fprintf(fp,"%.16f %.16f\n",re[i], im[i]);
	}
	fclose(fp);
	return;
}

void fprint_float_array(char* file_name, float* array, int size)
{
        int i;
        FILE* fp;
        fp = fopen( file_name, "w" );
        for (i=0;i<size;i++)
        {
                fprintf(fp,"%f\n",array[i]);
        }
        fclose(fp);
        return;
}

//A quick function that prints an mxm matrix to the file file_name.  Columns are separated by tabs and rows by new lines.
void fprint_square_matrix(char* file_name, double* array, int length)
{
        int i, j;
        FILE* fp;
	fp = fopen( file_name, "w" );
        for (i=0;i<length;i++)
        {
		for (j=0;j<length;j++)
		{
                	fprintf(fp,"%lf\t",array[i*length+j]);
		}
		fprintf(fp,"\n");
        }
        fclose(fp);
        return;
}

