#!/bin/bash

echo -e "\033[32mMake libfmft\033[0m"
cd libfmft
make clean
make all
make install-local
cd ..

echo -e "\033[32mMake fmft_dock\033[0m"
cd fmft_dock
make clean
make all
make install-local
cd ..

echo -e "\033[32mConfigure and compile pdb2pqr\033[0m"
cd install-local/pdb_prep/pdb2pqr-1.9.0
python scons/scons.py
cd ../../../

echo -e "\033[32mMake libmol-dependent\033[0m"
cd libmol_dependent
./bootstrap_libmol.sh

echo -e "\033[32mDone!\033[0m"


