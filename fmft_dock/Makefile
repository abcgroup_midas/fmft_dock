_os = $(shell uname)
BUILD_DIR=./build
SRC_DIR=./src
OBJ_DIR=./obj
INSTALL_DIR_LOCAL=../install-local

CC=mpiCC

CFLAGS := -O2 -Wall -W -Wshadow -Wpointer-arith -Wcast-qual -L/usr/lib64 -fPIC -mtune=native -march=native -g $(CFLAGS) #-Wno-unused-result -pg -std=gnu99 -fopenmp -ftree-vectorize -msse2 -ftree-vectorizer-verbose=1
CPPFLAGS := -I./src -I$(INSTALL_DIR_LOCAL)/include/libfmft -I$(INSTALL_DIR_LOCAL)/include/libfmft_grid $(CPPFLAGS)
LDFLAGS := -L$(INSTALL_DIR_LOCAL)/lib -Wl,-z,origin -Wl,-rpath='$$ORIGIN/lib' -Wl,-rpath='$$ORIGIN/../lib' #specifying the library position relative to executable
LIBS := -lm -lgmpxx -lgmp -lfftw3 -lfftw3f

CPPFLAGS := -D _MPI_ -D _SINGLE_PREC_ $(CPPFLAGS)   

TTM_OBJS = $(OBJ_DIR)/tab_trans_matrix.o
SCCFFS_OBJS = $(OBJ_DIR)/scale_coeffs.o
G2C_OBJS = $(OBJ_DIR)/grids_to_coeffs.o
LWRDRSCN_OBJS = $(OBJ_DIR)/low_order_scan.o
FMFTDOCK_OBJS =  $(OBJ_DIR)/fmft_dock.o

$(BUILD_DIR)/utils/tab_trans_matrix: $(TTM_OBJS)
	$(CC) $(CFLAGS) $(CPPFLAGS) $(LDFLAGS) $(TTM_OBJS) -lfmft -o $@

$(BUILD_DIR)/utils/scale_coeffs:  $(SCCFFS_OBJS)
	$(CC) $(CFLAGS) $(CPPFLAGS) $(LDFLAGS) $(SCCFFS_OBJS) $(LIBS) -lfmft -lfmft_grid -o $@
	
$(BUILD_DIR)/utils/grids_to_coeffs:  $(G2C_OBJS)
	$(CC) $(CFLAGS) $(CPPFLAGS) $(LDFLAGS) $(G2C_OBJS) $(LIBS) -lfmft -lfmft_grid -o $@
	
$(BUILD_DIR)/utils/low_order_scan: $(LWRDRSCN_OBJS)
	$(CC) $(CFLAGS) $(CPPFLAGS) $(LDFLAGS) $(LWRDRSCN_OBJS) $(LIBS) -lfmft -o $@

$(BUILD_DIR)/fmft_dock: $(FMFTDOCK_OBJS)
	$(CC) $(CFLAGS) $(CPPFLAGS) $(LDFLAGS) $(FMFTDOCK_OBJS) $(LIBS) -lfmft -o $@
	
$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c
	if [ ! -d ${OBJ_DIR} ]; then mkdir ${OBJ_DIR}; fi
	if [ ! -d ${BUILD_DIR} ]; then mkdir ${BUILD_DIR}; fi
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $< -o $@

$(OBJ_DIR)/%.o: $(SRC_DIR)/utils/%.c
	if [ ! -d ${OBJ_DIR} ]; then mkdir ${OBJ_DIR}; fi
	if [ ! -d ${BUILD_DIR}/utils ]; then mkdir ${BUILD_DIR}/utils; fi
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $< -o $@

#fmft_dock: $(BUILD_DIR)/fmft_dock
	
utils: $(BUILD_DIR)/utils/tab_trans_matrix\
		$(BUILD_DIR)/utils/low_order_scan

all: $(BUILD_DIR)/fmft_dock utils 

install-local: 
	mkdir -p $(INSTALL_DIR_LOCAL)
	cp -r $(BUILD_DIR)/* $(INSTALL_DIR_LOCAL)
	cp -r scripts/*.sh $(INSTALL_DIR_LOCAL)
	cp -r scripts/*.pl $(INSTALL_DIR_LOCAL)
	cp -r scripts/prms $(INSTALL_DIR_LOCAL)
	cp -r scripts/pdb_prep $(INSTALL_DIR_LOCAL)
	cp -r precomp_binaries/* $(INSTALL_DIR_LOCAL)
	
clean:
	$(RM) $(OBJ_DIR)/*.o
	$(RM) $(BUILD_DIR)/fmft_dock
	$(RM) $(BUILD_DIR)/utils/tab_trans_matrix 
	#$(RM) $(BUILD_DIR)/utils/scale_coeffs 
	#$(RM) $(BUILD_DIR)/utils/grids_to_coeffs 
	$(RM) $(BUILD_DIR)/utils/low_order_scan



.PHONY: all clean lib utils
.DEFAULT_GOAL := all

