/******************************************************************************
Copyright (c) 2015 Dzmitry Padhorny, Andrey Kazennov and Dima Kozakov

Permission is hereby granted, free of charge, to any person obtaining a copy 
of this software and associated documentation files (the "Software"), to deal 
in the Software without restriction, including without limitation the rights 
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
SOFTWARE.
*******************************************************************************/
#include "libfmft.h"
#include <errno.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>
#define _USE_MATH_DEFINES
#define _MY_ROOT_ 0
#define DATA_FOLDER "data"
//#define OLD_CODE
#include <libgen.h>
#include <fenv.h>
#include <time.h>
#include <fftw3.h>

#ifdef _MPI_
#include "mpi.h"
#endif

#ifdef _MPI_

MPI_Datatype mpi_type_score ()
{
	struct Score sendscore[1];
	MPI_Datatype MPI_SCORE;
	MPI_Datatype types[7] = {MPI_DOUBLE, MPI_INT, MPI_INT, MPI_INT, MPI_INT, MPI_INT, MPI_DOUBLE};
	int blocklens[7] = {1, 1, 1, 1, 1, 1, 1};
	MPI_Aint addrs[8];
	MPI_Aint disps[7];

	/* Calc the offset of elements relative to sendscore */

	MPI_Address(sendscore, &addrs[0]);
	MPI_Address(&(sendscore->val),    &addrs[1]);
	MPI_Address(&(sendscore->beta),   &addrs[2]);
	MPI_Address(&(sendscore->gamma),  &addrs[3]);
	MPI_Address(&(sendscore->alpha1), &addrs[4]);
	MPI_Address(&(sendscore->beta1),  &addrs[5]);
	MPI_Address(&(sendscore->gamma1), &addrs[6]);
	MPI_Address(&(sendscore->Z),      &addrs[7]);

	disps[0] = addrs[1] - addrs[0];
	disps[1] = addrs[2] - addrs[0];
	disps[2] = addrs[3] - addrs[0];
	disps[3] = addrs[4] - addrs[0];
	disps[4] = addrs[5] - addrs[0];
	disps[5] = addrs[6] - addrs[0];
	disps[6] = addrs[7] - addrs[0];

	MPI_Type_struct (7, blocklens, disps, types, &MPI_SCORE);
	MPI_Type_commit (&MPI_SCORE);

	return MPI_SCORE;
}
#endif

//#define theta M_PI* 30. / 180.

/************************************************************************************************\
* This file contains an example of docking routine,                                              *
* Usage:                                                                                         *
* ./fmft_dock coeffs_file output_file rmatrix_file nresults Zstart Zstop Zstep theta FFT_size    *
*                                                                                                *
* Where                                                                                          *
*        coeffs_file [filename]    -- path to the file containing spf expansion                  *
*                                     coefficients                                               *
*        output_file [filename]    -- path to the output file, to which                          *
*                                     the scored and filtered                                    *
*                                     conformations will be written                              *
*        rmatrix_file [filename]   -- path to the file to which the                              *
*                                     rotation matrices will be written                          *
*        nresults [integer]        -- Number of best-scoring poses to be                         *
*                                     presented in the output file                               *
*        Zstart      [angstrom]    -- Minimal translation distance                               *
*        Zstop       [angstrom]    -- Maximal translation distance                               *
*                                     (no reason to make it > 60 Angstrom)                       *
*        Zstep       [angstrom]    -- Translation step (typically 1.0 Angstrom)                  *
*        theta       [degrees]     -- Minimum distance between scoring                           *
*                                     conformations                                              *
*                                                                                                *
*                                                                                                *
\************************************************************************************************/

int main(int argc, char *argv[])
{
    feenableexcept(FE_DIVBYZERO | FE_INVALID | FE_OVERFLOW);
    if (argc != 10)
    {
        printf("Usage: %s coeffs_file output_file rmatrix_file nresults Zstart Zstop Zstep theta FFT_size\n", argv[0]);
        exit(EXIT_FAILURE);
    }
	int myrank = 0; // rank id of process
	int nprocs = 1; // the number of processes
	int myroot = 0; //

#ifdef _MPI_
	if ((MPI_Init(&argc, &argv)) != MPI_SUCCESS)
	{
		(void) fprintf (stderr, "MPI_Init failed\n");
		MPI_Abort (MPI_COMM_WORLD, 1);
	}
	if (MPI_Comm_rank(MPI_COMM_WORLD, &myrank) != MPI_SUCCESS)
		MPI_Abort (MPI_COMM_WORLD, 1);
	if (MPI_Comm_size(MPI_COMM_WORLD, &nprocs) != MPI_SUCCESS)
		MPI_Abort (MPI_COMM_WORLD, 1);
#endif

    char*  coeffs_file  = argv[1];      // generalized Fourier coefficients file
    char*  output_file  = argv[2];      // output file, which will contain a table of the best energy scores
    char*  rmatrix_file = argv[3];      // output file, which will contain a table of rotation matrices
    int    nresults = atoi(argv[4]); // Number of best scores processed for each translation step
    double Z_start = atof(argv[5]);
    double Z_stop  = atof(argv[6]);
    double Z_step  = atof(argv[7]);
    double theta   = atof(argv[8]) * M_PI / 180.0;
	int FFT_size = atoi(argv[9]);
    char*  folder_name = dirname(argv[0]); //TODO Change to a more safe variant.

    //read spf coefficients
    struct many_spf_coeffs* mcoeffs = read_many_spf_coeffs(coeffs_file);
    struct vector3 ref_lig_pos;
    //get ligand position relative to receptor
    ref_lig_pos.X = mcoeffs->lig_origin->X - mcoeffs->rec_origin->X;
    ref_lig_pos.Y = mcoeffs->lig_origin->Y - mcoeffs->rec_origin->Y;
    ref_lig_pos.Z = mcoeffs->lig_origin->Z - mcoeffs->rec_origin->Z;

	my_printf(myrank, "Using spf coefficients calculated up to N = %d for %d grid pairs\n", mcoeffs->N, mcoeffs->count);
	my_printf(myrank, "Using FFT grid with dimensions: %d x %d x (%d x %d x %d)\n", FFT_size / 2 + 1, FFT_size / 2 + 1, FFT_size, FFT_size, FFT_size);
	my_printf(myrank, "Using cutoff of %.2lf deg. for clustering. (Largest grid step is %.2lf deg.)\n", theta * 180.0 / M_PI, 180.0 / ((int)FFT_size / 2) );
	my_printf(myrank, "\"Raw\" translation range: %.2lf - %.2lf Angstrom, step: %.2lf Angstrom\n", Z_start, Z_stop, Z_step);
	my_printf(myrank, "Results will be provided relative to the reference ligand position: %lf %lf %lf\n", ref_lig_pos.X, ref_lig_pos.Y, ref_lig_pos.Z);


    /***SCORE_AND_FILTER***/
    int trs_tot = floor((Z_stop - Z_start) / Z_step + 1); // total number of translation steps;
    int trs_per_proc = trs_tot / nprocs + 1;              // number of translation steps per core
	my_printf(myrank, "The task will be completed using %d cpu cores.\n", nprocs);
	my_printf(myrank, "Each core will handle %d translation steps.\n", trs_per_proc);
	my_printf(myrank, "Padded translation range: %.2lf - %.2lf Angstrom.\n", Z_start, Z_start + (trs_per_proc * nprocs - 1) * Z_step);

    double loc_Z_start = Z_start + myrank * trs_per_proc * Z_step;  // translation range lower boundary for the current core
    double loc_Z_stop  = loc_Z_start + (trs_per_proc - 1) * Z_step; // translation range upper boundary for the current core

    int loc_output_scores_count = trs_per_proc * nresults; // number of scores for the current core
    struct Score* loc_output_scores = (struct Score*) malloc(loc_output_scores_count * sizeof(struct Score));

    score_and_cluster_v1(mcoeffs,
    		loc_output_scores,
    		FFT_size,
    		loc_Z_start,
    		loc_Z_stop,
    		Z_step,
    		nresults,
    		theta,
    		folder_name,
    		nprocs,
    		myrank);

    /***COLLECT_RESULTS***/
    int output_scores_count = loc_output_scores_count * nprocs; // total number of scores
    struct Score* output_scores;
#ifdef _MPI_
    if (myrank == myroot)
    {
    	output_scores = (struct Score*) malloc(output_scores_count * sizeof(struct Score));
    }

	MPI_Datatype MPI_SCORE = mpi_type_score ();

	MPI_Barrier(MPI_COMM_WORLD);
    MPI_Gather(loc_output_scores, loc_output_scores_count, MPI_SCORE,
        output_scores, loc_output_scores_count, MPI_SCORE, myroot,
        MPI_COMM_WORLD);
	MPI_Barrier(MPI_COMM_WORLD);

	MPI_Type_free (&MPI_SCORE);
#else
	output_scores = loc_output_scores;
#endif

    if (myrank == myroot)
    {
        /***SORT RESULTS***/
        output_sort(output_scores, output_scores_count, output_scores_count);

        /***SAVE RESULTS IN PIPER-LIKE FORM***/
        save_score_piper(output_scores, output_scores_count, FFT_size, ref_lig_pos, output_file, rmatrix_file);
    	free(output_scores);
    }
	deallocate_many_spf_coeffs(mcoeffs);
	free(loc_output_scores);

	/***MPI FINALIZE***/
#ifdef _MPI_
	if ((MPI_Finalize()) != MPI_SUCCESS)
	{
		fprintf (stderr, "MPI_Finalize failed\n");
		MPI_Abort (MPI_COMM_WORLD, 1);
	}
#endif
    return 0;
}
