/******************************************************************************
Copyright (c) 2015 Dzmitry Padhorny, Andrey Kazennov and Dima Kozakov

Permission is hereby granted, free of charge, to any person obtaining a copy 
of this software and associated documentation files (the "Software"), to deal 
in the Software without restriction, including without limitation the rights 
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
SOFTWARE.
*******************************************************************************/
#include "libfmft.h"
#include <errno.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define _USE_MATH_DEFINES
#include <libgen.h>
#include <fenv.h>

#ifdef _MPI_
#include "mpi.h"
#endif

#ifdef _MPI_

MPI_Datatype mpi_type_score ()
{
	struct Score sendscore[1];
	MPI_Datatype MPI_SCORE;
	MPI_Datatype types[7] = {MPI_DOUBLE, MPI_INT, MPI_INT, MPI_INT, MPI_INT, MPI_INT, MPI_DOUBLE};
	int blocklens[7] = {1, 1, 1, 1, 1, 1, 1};
	MPI_Aint addrs[8];
	MPI_Aint disps[7];

	/* Вычислить смещения элементов
	 * относительно sendscore */
	MPI_Address(sendscore, &addrs[0]);
	MPI_Address(&(sendscore->val),    &addrs[1]);
	MPI_Address(&(sendscore->beta),   &addrs[2]);
	MPI_Address(&(sendscore->gamma),  &addrs[3]);
	MPI_Address(&(sendscore->alpha1), &addrs[4]);
	MPI_Address(&(sendscore->beta1),  &addrs[5]);
	MPI_Address(&(sendscore->gamma1), &addrs[6]);
	MPI_Address(&(sendscore->Z),      &addrs[7]);

	disps[0] = addrs[1] - addrs[0];
	disps[1] = addrs[2] - addrs[0];
	disps[2] = addrs[3] - addrs[0];
	disps[3] = addrs[4] - addrs[0];
	disps[4] = addrs[5] - addrs[0];
	disps[5] = addrs[6] - addrs[0];
	disps[6] = addrs[7] - addrs[0];

	MPI_Type_struct (7, blocklens, disps, types, &MPI_SCORE);
	MPI_Type_commit (&MPI_SCORE);

	return MPI_SCORE;
}
#endif

void find_opt_tr_range (const struct Score* scores, const int num_scores, double* tr_range_start, double* tr_range_stop)
{
	//determine minimum score and score at infinity
	//(the latter is technically the score at largest tr.dist, and should be close to zero)

	double max = 0.0; //scores[num_scores - 1].val;
	double min = scores[0].val;
	for (int i = 0; i < num_scores; ++i)
	{
		if (scores[i].val < min)
			min = scores[i].val;
	}

	//find tr. range.
	double Z_start, Z_stop;

	//find the left boundary (the first point with score less than that at infinity).
	int pos = 0;
	do
	{
		Z_start = scores[pos].Z;
		++pos;
	}
	while ((pos < num_scores) && (scores[pos - 1].val > max));

	int left_id = pos -1;

	//now find the average score value of the region between the left boundary and the largest translation distance.
	double sum = 0;
	for (int i = left_id; i < num_scores; ++i)
	{
		sum += scores[i].val;
	}
	double average = sum / (num_scores - left_id);

	//find the right boundary using the computed average as a cutoff value.
	pos = num_scores - 1;
	do
	{
		Z_stop = scores[pos].Z;
		--pos;
	}
	while ((pos >= 0) && (scores[pos + 1].val > average));

	//Check if this tr. range makes any sense.
	if (Z_stop < Z_start)
	{
		fprintf (stderr, "Filed to determine the translation range, exiting.\n");
		exit(0);
	}

	*tr_range_start = Z_start;
	*tr_range_stop  = Z_stop + 5.00; // empirically chosen padding
}


/*
 * Performs low order scan of protein interaction energy landscape to determine the optimal translation range for a high order search.
 *
 * command line format:
 * ./low_order_scan coeffs_file output_file Z_start Z_stop Z_step FFT_size
 *
 * Z_start and Z_stop here are initial guesses for minimum and maximum translation distances. They can be based on protein shapes, for example.
 */

int main(int argc, char *argv[])
{
    feenableexcept(FE_DIVBYZERO | FE_INVALID | FE_OVERFLOW);
    if (argc != 8)
    {
        printf("Usage: %s coeffs_file output_file Zstart Zstop Zstep FFT_size\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    /***INITIALIZE MPI***/
	int myrank = 0; // rank id of process
	int nprocs = 1; // the number of processes
	int myroot = 0;
#ifdef _MPI_
	if ((MPI_Init(&argc, &argv)) != MPI_SUCCESS)
	{
		(void) fprintf (stderr, "MPI_Init failed\n");
		MPI_Abort (MPI_COMM_WORLD, 1);
	}
	if (MPI_Comm_rank(MPI_COMM_WORLD, &myrank) != MPI_SUCCESS)
		MPI_Abort (MPI_COMM_WORLD, 1);
	if (MPI_Comm_size(MPI_COMM_WORLD, &nprocs) != MPI_SUCCESS)
		MPI_Abort (MPI_COMM_WORLD, 1);
#endif

    char*  coeffs_file = argv[1];
    char*  output_file = argv[2];
    double Z_start = atof(argv[3]);
    double Z_stop  = atof(argv[4]);
    double Z_step  = atof(argv[5]);
    int N          = atoi(argv[6]);
	int FFT_size   = atoi(argv[7]);
    char*  folder_name = dirname(dirname(argv[0])); //TODO Change to a more safe variant.

    /**READ SPF COEFFICIENTS***/
    struct many_spf_coeffs* mcoeffs_ho = read_many_spf_coeffs(coeffs_file);
    struct many_spf_coeffs* mcoeffs = get_many_spf_coeffs_low_order(mcoeffs_ho, 20);

	my_printf(myrank, "Using spf coefficients calculated up to N = %d for %d grid pairs\n", mcoeffs->N, mcoeffs->count);
	my_printf(myrank, "Using FFT grid with dimensions: %d x %d x (%d x %d x %d)\n", FFT_size / 2 + 1, FFT_size / 2 + 1, FFT_size, FFT_size, FFT_size);
	my_printf(myrank, "(Largest grid step is %.2lf deg.)\n", 180.0 / ((int)FFT_size / 2) );
	my_printf(myrank, "\"Raw\" translation range: %.2lf - %.2lf Angstrom, step: %.2lf Angstrom\n", Z_start, Z_stop, Z_step);


    /*** SCORE AND CLUSTER***/
    int score_count = 1;
    int trs_tot = floor((Z_stop - Z_start) / Z_step + 1); // total number of translation steps;
    int trs_per_proc = trs_tot / nprocs + 1;          // number of translation steps per core
	my_printf(myrank, "The task will be completed using %d cpu cores.\n", nprocs);
	my_printf(myrank, "Each core will handle %d translation steps.\n", trs_per_proc);
	my_printf(myrank, "Padded translation range: %.2lf - %.2lf Angstrom.\n", Z_start, Z_start + (trs_per_proc * nprocs - 1) * Z_step);

    double loc_Z_start = Z_start + myrank * trs_per_proc * Z_step;
    double loc_Z_stop  = loc_Z_start + (trs_per_proc - 1) * Z_step;

    int loc_output_scores_count = trs_per_proc * score_count;
    struct Score* loc_output_scores = (struct Score*) malloc(loc_output_scores_count * sizeof(struct Score));

    score_and_cluster_v1(mcoeffs,\
    		loc_output_scores,\
    		FFT_size,\
    		loc_Z_start,\
    		loc_Z_stop,\
    		Z_step,\
    		score_count /* we take a single score from each translation step*/,\
    		1.0 /*cutoff value doesn't really matter, since we take a single score*/,\
    		folder_name,
    		nprocs,
    		myrank);

    /***COLLECT_RESULTS***/
    int output_scores_count = loc_output_scores_count * nprocs; // total number of scores
    struct Score* output_scores; // array containing all scores
#ifdef _MPI_
    if (myrank == myroot)
    {
    	output_scores = (struct Score*) malloc(output_scores_count * sizeof(struct Score));
    }

	MPI_Datatype MPI_SCORE = mpi_type_score ();

	MPI_Barrier(MPI_COMM_WORLD);
    MPI_Gather(loc_output_scores, loc_output_scores_count, MPI_SCORE,
        output_scores, loc_output_scores_count, MPI_SCORE, myroot,
        MPI_COMM_WORLD);
	MPI_Barrier(MPI_COMM_WORLD);

	MPI_Type_free (&MPI_SCORE);
#else
   	output_scores = loc_output_scores;
#endif

   	/***FIND OPTIMAL TRANSLATION RANGE***/
   	double new_Z_start, new_Z_stop;
   	if (myrank == myroot)
   	{
   		find_opt_tr_range(output_scores, output_scores_count, &new_Z_start, &new_Z_stop);
   		//printf("optimal translation range:\n Z_start = %.2lf, Z_stop = %.2lf\n", new_Z_start, new_Z_stop);
   		FILE* f = fopen(output_file, "w");
   		printf("Optimal tr.range: %.2lf - %.2lf Angstrom\n", new_Z_start, new_Z_stop);
   		fprintf(f, "%.2lf %.2lf\n", new_Z_start, new_Z_stop);
   		fclose(f);
   	}

   	/***DEALLOCATE MEMORY***/
    if (myrank == myroot)
    {
    	free(output_scores);
    }
    deallocate_many_spf_coeffs(mcoeffs);
	free(loc_output_scores);

	/***MPI FINALIZE***/
#ifdef _MPI_
	if ((MPI_Finalize()) != MPI_SUCCESS)
	{
		fprintf (stderr, "MPI_Finalize failed\n");
		MPI_Abort (MPI_COMM_WORLD, 1);
	}
#endif
    return 0;
}
