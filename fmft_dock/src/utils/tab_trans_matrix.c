/******************************************************************************
Copyright (c) 2015 Dzmitry Padhorny, Andrey Kazennov and Dima Kozakov

Permission is hereby granted, free of charge, to any person obtaining a copy 
of this software and associated documentation files (the "Software"), to deal 
in the Software without restriction, including without limitation the rights 
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
SOFTWARE.
*******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <libgen.h>
#include <unistd.h>
#include "libfmft.h"


/*
 * ./tab_trans_matrix Z_start Z_stop Z_step
 */
int main(int argc, char **argv)
{
    clock_t t1, t2;

    if (argc != 4)
    {
        printf("Usage %s Z_start Z_stop Z_step\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    char*   folder_name = dirname(dirname(argv[0]));
    float Z_start = atof(argv[1]);
    float Z_stop  = atof(argv[2]);
    float Z_step  = atof(argv[3]);

    char T_file[1024];

    for (double z = Z_start; z <= Z_stop; z += Z_step)
    {

        sprintf(T_file, "%s/%s/%s_%d_%.2lf.dat", folder_name, TFOLDER, TFILE, NMAX, z);
        if (access(T_file, F_OK) == -1)
        {
        	printf("Calculating translation matrix for z = %.2lf:\n", z);
        	t1 = clock();
        	tab_T_array(NMAX, z, folder_name);
        	t2 = clock();
        	printf("time: %lf\n", 1.0*(t2 - t1)/CLOCKS_PER_SEC );
        }
        else
        {
        	printf("File %s already exists, skipping.\n", T_file);
        }

    }

    printf("Done.\n");

    return 0;
}
