#!/bin/bash
#set -euo pipefail
# This script executes the docking routine for a given receptor-ligand pair.
# receptor and ligand pdb files are supposed to be pre-processed.
# All temporary files and results are stored in the current folder.

# Usage: ./dock_pair.sh rec.pdb lig.pdb weights.txt [optional arguments]

if [ "$#" -lt 3 ]; then
	echo "Illegal number of arguments"
	echo "usage:"
	echo "./dock_pair.sh rec.pdb lig.pdb weights.txt [optional arguments]"

        echo "optional arguments:"
	echo "-p   |--proc_count     (ex: 4)                Number of CPU cores to use (default: 1)"
	echo "-c   |--cluster                               Perform clustering of the results with default parameters"
	echo "-r   |--ref_lig        (ex: ref_lig.pdb)      Reference ligand for RMSD calculation"
	echo "-o   |--others                                Others mode"
	echo "-n   |--num_res        (ex: 1000)             Number of top results from ft file to process (default: 1000)"

	exit 1
fi

#path to scripts & binaries (all supposed to be found in the same dir)
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

#get required arguments
REC_PDB=${1}	#receptor pdb file
shift
LIG_PDB=${1}	#ligand pdb file
shift
WEIGHTS=${1}	#weights file
shift

#fixed paramenters:
N=30		#polynomial order of expansions.
FFT_SIZE=59	#FFT grid size in each dimension. FFT_SIZE = 2 * N - 1 is to be used as the default option..
LO_N=20		#spf depth used for low order scan
LO_FFT_SIZE=30	#FFT size  used for low order scan
CUTOFF=6.0	#filtering parameter for dealing with nonuniformity of the sampling procedure 
		#(should be a bit smaller than the largest grid step)
#CUTOFF=$( python -c "print 0.98 * 360 / ${FFT_SIZE}" ); 
RAW_Z_START=0	#translation range start
RAW_Z_STOP=60	#translation range stop (quality of representation becomes too bad for values >60)
Z_STEP=1.0	#translation step value

#parce optional arguments
PROC_COUNT=1
CLUSTER=0
REF_LIG_PDB=""
OTHERS=0
NRES=1000	#number of poses in the output (also number of results saved for each translation step)
while [[ $# > 0 ]]
do
    key="$1"
    case $key in
        -p|--proc_count)
            PROC_COUNT=${2}
            shift 2 # past argument
        ;;
        -c|--cluster)
            CLUSTER=1
            shift   # past argument
        ;;
        -r|--ref_lig)
            REF_LIG_PDB=${2}
            shift 2 # past argument
        ;;
        -o|--others)
            OTHERS=1
            shift # past argument
        ;;
        -n|--num_res)
            NRES=${2}
            shift 2 # past argument
        ;;
        *)
            echo "unknown option: ${1}"; exit  # unknown option
        ;;
    esac
done
echo "PROC_COUNT=${PROC_COUNT}"
echo "CLUSTER=${CLUSTER}"
echo "REF_LIG_PDB=${REF_LIG_PDB}"

#filenames to use
GRIDS_FILE="grids.txt"
MCOEFFS_FILE="mcoeffs"
TR_RANGE_TEMPLATE="tr_range"
FT_TEMPLATE="ft"
RM_TEMPLATE="rm"

#prepare grids (creates a ${GRIDS_FILE})
echo -e "\033[32mPreparing grids:\033[0m"
time "${DIR}"/prep_grids.sh ${GRIDS_FILE} ${REC_PDB} ${LIG_PDB}

#transform grids to coefficients
echo -e "\033[32mConverting grids to generalized Fourier coefficients\033[0m"
"${DIR}"/utils/grids_to_coeffs ${GRIDS_FILE} ${N} ${MCOEFFS_FILE}
rm -f ./*.dx

if [ ! -f ${MCOEFFS_FILE} ]; then
	echo "${MCOEFFS_FILE} not found, aborting"
	exit 1
fi

#echo -e "\033[32mGenerate translation matrices in range ${RAW_Z_RANGE}\033[0m"
#./gen_trans_matrices.sh ${RAW_Z_START} ${RAW_Z_STOP} ${Z_STEP}

#read weights from file (in fact only the label - the first number in each line - is used)
WARR=();
index=0
while read line; do
    if [ ! -z "$line" ]; then
	WARR[${index}]="${line}"
	index=$(($index+1))
    fi
done < ${WEIGHTS}

#iterate weight sets
for ((i=0; i < ${#WARR[@]}; i++));
do
    WEIGHT_ID=$(echo ${WARR[${i}]} | awk '{print $1}')
    SUFFIX_A=$(printf "%03d" ${WEIGHT_ID})
    SUFFIX_B="00"

    TR_RANGE_FILE="${TR_RANGE_TEMPLATE}.${SUFFIX_A}.${SUFFIX_B}"
    FT_FILE="${FT_TEMPLATE}.${SUFFIX_A}.${SUFFIX_B}"
    RM_FILE="${RM_TEMPLATE}.${SUFFIX_A}.${SUFFIX_B}"

    #scale coefficients
    MCOEFFS_SCALED_FILE="${MCOEFFS_FILE}.${SUFFIX_A}.${SUFFIX_B}"
    "${DIR}"/utils/scale_coeffs ${MCOEFFS_SCALED_FILE} ${MCOEFFS_FILE} ${WEIGHTS} ${WEIGHT_ID}

    #get fine translation range using low-order scan on a small FFT grid
    echo -e "\033[32mIdentifying optimal translation range using low-order scan\033[0m"
    echo -e     "\033[33m Running ${DIR}/utils/low_order_scan ${MCOEFFS_SCALED_FILE} ${TR_RANGE_FILE} ${RAW_Z_START} ${RAW_Z_STOP} ${Z_STEP} ${LO_N} ${LO_FFT_SIZE}\033[0m"
    time mpirun -np ${PROC_COUNT} "${DIR}"/utils/low_order_scan ${MCOEFFS_SCALED_FILE} ${TR_RANGE_FILE} ${RAW_Z_START} ${RAW_Z_STOP} ${Z_STEP} ${LO_N} ${LO_FFT_SIZE}
    OPT_Z_RANGE=$(cat ${TR_RANGE_FILE} | tail -n 1)
    OPT_Z_START=$(echo ${OPT_Z_RANGE} | awk '{print $1}')
    OPT_Z_STOP=$(echo ${OPT_Z_RANGE} | awk '{print $2}')

    #dock
    echo -e "\033[32mStarting main docking routine for" ${MCOEFFS_SCALED_FILE} "\033[0m"
    echo -e     "\033[33m Running ${DIR}/fmft_dock ${MCOEFFS_SCALED_FILE} ${FT_FILE} ${RM_FILE} ${NRES} ${OPT_Z_START} ${OPT_Z_STOP} ${Z_STEP} ${CUTOFF} ${FFT_SIZE}\033[0m"
    time mpirun -np ${PROC_COUNT} "${DIR}"/fmft_dock ${MCOEFFS_SCALED_FILE} ${FT_FILE} ${RM_FILE} ${NRES} ${OPT_Z_START} ${OPT_Z_STOP} ${Z_STEP} ${CUTOFF} ${FFT_SIZE}
done


#optional actions

#cluster
OPT_PRMS_STR=""
if [ ${OTHERS} != 0 ]; then
    OPT_PRMS_STR="${OPT_PRMS_STR} -o"
fi

if [ "${REF_LIG_PDB}" != "" ]; then
    OPT_PRMS_STR="${OPT_PRMS_STR} -r ${REF_LIG_PDB}"
fi

if [ ${CLUSTER} != 0 ]; then
    bash "${DIR}"/cluster_pair.sh ${REC_PDB} ${LIG_PDB} ${WEIGHTS} ${OPT_PRMS_STR}
fi
