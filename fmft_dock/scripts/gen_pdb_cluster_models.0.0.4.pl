#! /usr/bin/perl -w
use strict;
use File::Basename;

if (scalar @ARGV < 6)
{
	print "usage: $0  .CLUSTERFILE  FTDATFILE.SORTED  ROTPRM  PDB_IFILE  OFILE_PRE  MAX_MODELS\n";
	exit;
}

my $dir = dirname(__FILE__);
# command line args
my $clusterfile = shift @ARGV;
my $ftdatfile = shift @ARGV;
my $rotprm = shift @ARGV;
my $lig = shift @ARGV;
my $ofilepre = shift @ARGV;
my $max_models = shift @ARGV;

open FTDATFILE, "< $ftdatfile" or die "couldn't open $ftdatfile: $!\n";
my @ftdatlines = <FTDATFILE>;
close FTDATFILE;
chomp @ftdatlines;

my $ofile = "$clusterfile.energies";
open OFILE, "> $ofile" or die "couldn't open $ofile: $!\n";

#my @centers = `grep Center $clusterfile`;
my @centers = `cat $clusterfile | awk '{print \$1}'`;
chomp @centers;

my $modeli = 0;
for my $center (@centers)
{
	#$center =~ s/^\s+//;
	#my @words = split /\s+/, $center;
	#my $fti = $words[1] - 1; # 0 based
	my $fti = $center - 1; # 0 based
	print "ftline: $fti\n";
	# keep a record of the energies
	my $ftline = $ftdatlines[$fti];
	print OFILE "$ftline\n";

#print "$center\n";
#print "$fti\n";
#print "$ftline\n";

	
	print "constructing model $modeli\n";

	my $lig_ofile = sprintf ("%s.%02d.pdb", $ofilepre, $modeli);
#print "ofile: $lig_ofile\n";

	system "$dir/gen_moved_pdb.0.0.4.pl $rotprm $ftdatfile $fti $lig $lig_ofile";

	$modeli++;
	last if ($modeli >= $max_models);
}

close OFILE;
