#!/bin/bash
#set -euo pipefail
# This script performs the clustering of the results for a series of receptor-ligand pairs 
# located in subdirectories of the current directory.

# Usage: ./cluster_series.sh weights.txt [optional arguments]

if [ "$#" -lt 1 ]; then
	echo "Illegal number of arguments"
	echo "usage:"
	echo "./dock_series.sh weights.txt [optional arguments]"

        echo "optional arguments:"
	echo "-r   |--ref_lig_given  			    Will treat ligand pdb as reference ligand pdb"
	echo "-rprm|--rot_prm        (ex: rot70k.0.0.4.prm) Global Rotation matrix file (PIPER-style)"
	echo "-o   |--others                                Others mode"
	echo "-s   |--skip_clustering                       Number of results to take from EACH ft file for the Others mode"

	exit 1
fi

#path to scripts & binaries (all supposed to be found in the same dir)
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

#get required arguments
WEIGHTS=${1}	#weights file
shift
WEIGHTS_ABS="$( cd "$( dirname "${WEIGHTS}" )" && pwd )/$( basename "${WEIGHTS}" )"
echo ${WEIGHTS_ABS}
#parce optional arguments
CLUSTER=1
REF_LIG_GIVEN=0
ROT_PRM=""
OTHERS=0
while [[ $# > 0 ]]
do
    key="$1"
    case $key in
        -rprm|--rot_prm)
            ROT_PRM=${2}
            ROT_PRM_ABS="$( cd "$( dirname "${ROT_PRM}" )" && pwd )/$( basename "${ROT_PRM}" )"
            shift 2 # past argument
        ;;
        -s|--skip_clustering)
            CLUSTER=0
            shift   # past argument
        ;;
        -r|--ref_lig_given)
            REF_LIG_GIVEN=1
            shift   # past argument
        ;;
        -o|--others)
            OTHERS=1
            shift   # past argument
        ;;
        *)
            echo "unknown option: ${1}"; exit  # unknown option
        ;;
    esac
done

OPT_PRMS_STR=""
if [ "${ROT_PRM}" != "" ]; then
    OPT_PRMS_STR="${OPT_PRMS_STR} -rprm ${ROT_PRM_ABS}"
fi
if [ ${CLUSTER} != 1 ]; then
    OPT_PRMS_STR="${OPT_PRMS_STR} -s"
fi
if [ ${OTHERS} != 0 ]; then
    OPT_PRMS_STR="${OPT_PRMS_STR} -o"
fi

TARGETS=$(ls -d */ | sed 's#\/##g')
for TARGET in ${TARGETS};
do
    cd ${TARGET}
    REC_PDB="${TARGET}_r_nmin.pdb"
    LIG_PDB="${TARGET}_l_nmin.pdb"
    if [ ${REF_LIG_GIVEN} != 0 ]; then
	REF_LIG_PDB=${LIG_PDB}
        OPT_PRMS_STR="${OPT_PRMS_STR} -r ${REF_LIG_PDB}"
    fi
    bash "${DIR}"/cluster_pair.sh ${REC_PDB} ${LIG_PDB} "${WEIGHTS_ABS}" ${OPT_PRMS_STR}
    cd ..
done
