#!/bin/bash
#set -euo pipefail
# This script clusters the docking poses for a given receptor-ligand pair
# and constructs the cluster-center-based models.
# If the reference ligand pdb is provided, it will also calculate
# the rmsd values of docking poses and final models.
#
# Receptor and ligand pdb files are supposed to be pre-processed.
# ft and rm files are supposed to be found in the current folder.
# All temporary files and results are stored in the current folder as well.

# Usage: ./cluster_pair.sh rec.pdb lig.pdb weights.txt [optional arguments]

if [ "$#" -lt 3 ]; then
	echo "Illegal number of arguments"
	echo "usage:"
	echo "./dock_pair.sh rec.pdb lig.pdb weights.txt [optional arguments]"

        echo "optional arguments:"
	echo "-r   |--ref_lig        (ex: ref_lig.pdb)      Reference ligand for RMSD calculation"
	echo "-rprm|--rot_prm        (ex: rot70k.0.0.4.prm) Global Rotation matrix file (PIPER-style)"
	echo "-n   |--num_res        (ex: 1000)             Number of top results from ft file to process (default: 1000)"
	echo "-o   |--others                                Others mode (default: off)"
	echo "-no  |--num_res_others (ex: 500)              Number of results to take from EACH ft file for the Others mode (default: 500)"
	echo "-s   |--skip_clustering                       Skip clustering (only calculate RMSD)"

	exit 1
fi

#path to scripts & binaries (all supposed to be found in the same dir)
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

#get required arguments
REC_PDB=${1}	#receptor pdb file
shift
LIG_PDB=${1}	#ligand pdb file
shift
WEIGHTS=${1}	#weights file
shift

#parce optional arguments
NRES=1000	#number of poses from ft file to use
NRES_EACH=500
CLUSTER=1
OTHERS=0
REF_LIG_PDB=""
ROT_PRM=""
PROC_COUNT=1
while [[ $# > 0 ]]
do
    key="$1"
    case $key in
        -r|--ref_lig)
            REF_LIG_PDB=${2}
            shift 2 # past argument
        ;;
        -rprm|--rot_prm)
            ROT_PRM=${2}
            shift 2 # past argument
        ;;
        -n|--num_res)
            NRES=${2}
            shift 2 # past argument
        ;;
        -o|--others)
            OTHERS=1
            shift # past argument
        ;;
        -no|--num_res_others)
            NRES_EACH=${2}
            shift 2 # past argument
        ;;
        -s|--skip_clustering)
            CLUSTER=0
            shift # past argument
        ;;
        *)
            echo "unknown option: ${1}"; exit  # unknown option
        ;;
    esac
done

#filenames to use
FT_TEMPLATE="ft"
RM_TEMPLATE="rm"

#read weights from file (in fact only the label - the first number in each line - is used)
WARR=();
index=0
while read line; do
    if [ ! -z "$line" ]; then
	WARR[${index}]="${line}"
	index=$(($index+1))
    fi
done < ${WEIGHTS}

#create mixed ft and rm files for the Others-type approach
NRES_OTHERS=$((${NRES_EACH} * ${#WARR[@]}))
if [ ${OTHERS} != 0 ]; then
    WEIGHT_ID=${#WARR[@]}
    SUFFIX_A_MIX=$(printf "%03d" ${WEIGHT_ID})
    SUFFIX_B_MIX="00"
    FT_MIX_FILE="${FT_TEMPLATE}.${SUFFIX_A_MIX}.${SUFFIX_B_MIX}"
    RM_MIX_FILE="${RM_TEMPLATE}.${SUFFIX_A_MIX}.${SUFFIX_B_MIX}"
    rm -f ${FT_MIX_FILE}
    rm -f ${RM_MIX_FILE}
    #iterate weight sets
    for ((i=0; i < ${#WARR[@]}; i++));
    do
        WEIGHT_ID=$(echo ${WARR[${i}]} | awk '{print $1}')
        SUFFIX_A=$(printf "%03d" ${WEIGHT_ID})
        SUFFIX_B="00"

        FT_FILE="${FT_TEMPLATE}.${SUFFIX_A}.${SUFFIX_B}"
        RM_FILE="${RM_TEMPLATE}.${SUFFIX_A}.${SUFFIX_B}"
        if [ "${ROT_PRM}" != "" ]; then #if global rot_prm file is given (i.e. PIPER-style, not MFFT-style)
            head -n ${NRES_EACH} ${FT_FILE} | awk '{print $1, $2, $3, $4, $5, $6, $7, $8, $9, $10}' >> ${FT_MIX_FILE}
            #dont care about the RM_MIX_FILE in this case
	else
            head -n ${NRES_EACH} ${FT_FILE} | awk '{print $1+'$((${NRES_EACH}*${i}))', $2, $3, $4, $5, $6, $7, $8, $9, $10}' >> ${FT_MIX_FILE}
            head -n ${NRES_EACH} ${RM_FILE} | awk '{print $1+'$((${NRES_EACH}*${i}))', $2, $3, $4, $5, $6, $7, $8, $9, $10}' >> ${RM_MIX_FILE}
        fi
    done
fi

#calculate iRMSD relative to reference ligand
if [ "${REF_LIG_PDB}" != "" ]; then
    REC_NOH_PDB=$(echo ${REC_PDB} | sed "s#\.pdb#_noh.pdb#g")
    #remove hydrogens:
    cat ${REC_PDB} | awk '$3 !~ /^H/ { print $0 }' > ${REC_NOH_PDB}

    RMSD_TEMPLATE="rmsd"
    #iterate weight sets
    for ((i=0; i < ${#WARR[@]}; i++));
    do
        WEIGHT_ID=$(echo ${WARR[${i}]} | awk '{print $1}')
        SUFFIX_A=$(printf "%03d" ${WEIGHT_ID})
        SUFFIX_B="00"

        FT_FILE="${FT_TEMPLATE}.${SUFFIX_A}.${SUFFIX_B}"
        RM_FILE="${RM_TEMPLATE}.${SUFFIX_A}.${SUFFIX_B}"
        if [ "${ROT_PRM}" != "" ]; then #if global rot_prm file is given (i.e. PIPER-style)
            RM_FILE=${ROT_PRM}
        fi
        RMSD_FILE="${RMSD_TEMPLATE}.${SUFFIX_A}.${SUFFIX_B}"

        #calculate rmsd
        "${DIR}"/utils_libmol/rmsd ${REC_NOH_PDB} ${LIG_PDB} ${LIG_PDB} ${REF_LIG_PDB} "${DIR}/prms/atoms.0.0.4.prm.ms.3cap+0.5ace.Hr0rec" ${RM_FILE} ${FT_FILE} ${RMSD_FILE} ${NRES} 10 1
    done

    #Others option
    if [ ${OTHERS} != 0 ]; then
        WEIGHT_ID=${#WARR[@]}
        SUFFIX_A_MIX=$(printf "%03d" ${WEIGHT_ID})
        SUFFIX_B_MIX="00"

        FT_MIX_FILE="${FT_TEMPLATE}.${SUFFIX_A_MIX}.${SUFFIX_B_MIX}"
        RM_MIX_FILE="${RM_TEMPLATE}.${SUFFIX_A_MIX}.${SUFFIX_B_MIX}"
        if [ "${ROT_PRM}" != "" ]; then #if global rot_prm file is given (i.e. PIPER-style)
            RM_MIX_FILE=${ROT_PRM}
        fi
        RMSD_MIX_FILE="${RMSD_TEMPLATE}.${SUFFIX_A_MIX}.${SUFFIX_B_MIX}"

        #calculate rmsd
        "${DIR}"/utils_libmol/rmsd ${REC_NOH_PDB} ${LIG_PDB} ${LIG_PDB} ${REF_LIG_PDB} "${DIR}/prms/atoms.0.0.4.prm.ms.3cap+0.5ace.Hr0rec" ${RM_MIX_FILE} ${FT_MIX_FILE} ${RMSD_MIX_FILE} ${NRES_OTHERS} 10 1 
    fi
fi

#cluster and build models based on cluster centers
MAX_MODELS=30
if [ ${CLUSTER} == 1 ]; then
    RMSD_TEMPLATE="rmsd"
    NAT_CLUSTER_TEMPLATE="native_cluster"
    #iterate weight sets
    for ((i=0; i < ${#WARR[@]}; i++));
    do
        WEIGHT_ID=$(echo ${WARR[${i}]} | awk '{print $1}')
        SUFFIX_A=$(printf "%03d" ${WEIGHT_ID})
        SUFFIX_B="00"

        FT_FILE="${FT_TEMPLATE}.${SUFFIX_A}.${SUFFIX_B}"
        RM_FILE="${RM_TEMPLATE}.${SUFFIX_A}.${SUFFIX_B}"
        if [ "${ROT_PRM}" != "" ]; then #if global rot_prm file is given (i.e. PIPER-style)
            RM_FILE=${ROT_PRM}
        fi

	#construct pairwise rmsd matrix
        "${DIR}"/utils_libmol/pwrmsd ${REC_PDB} ${LIG_PDB} "${DIR}/prms/atoms.0.0.4.prm.ms.3cap+0.5ace.Hr0rec"  ${RM_FILE} "./" ${SUFFIX_A} ${NRES} 1 1 ${SUFFIX_B}
	#cluster
        "${DIR}"/utils_libmol/cluster -f "clustermat.${SUFFIX_A}.${SUFFIX_B}.1" -d "new" -n ${NRES} -u ${NRES} -l 50 -s 10  -p 1 -r 9

	#build models
	"${DIR}"/gen_pdb_cluster_models.0.0.4.pl "clustermat.${SUFFIX_A}.${SUFFIX_B}.1.cluster"  ${FT_FILE}  ${RM_FILE}  ${LIG_PDB}  "lig.${SUFFIX_A}"  ${MAX_MODELS}

	#get top model under 10 A rmsd
        if [ "${REF_LIG_PDB}" != "" ]; then
            RMSD_FILE="${RMSD_TEMPLATE}.${SUFFIX_A}.${SUFFIX_B}"
            NAT_CLUSTER_FILE="${NAT_CLUSTER_TEMPLATE}.${SUFFIX_A}.${SUFFIX_B}.txt"
            "${DIR}"/utils_libmol/cluster_number "clustermat.${SUFFIX_A}.${SUFFIX_B}.1.cluster" ${RMSD_FILE} ${NAT_CLUSTER_FILE}
	fi
    done

    #Others option
    if [ ${OTHERS} != 0 ]; then
        WEIGHT_ID=${#WARR[@]}
        SUFFIX_A_MIX=$(printf "%03d" ${WEIGHT_ID})
        SUFFIX_B_MIX="00"

        FT_MIX_FILE="${FT_TEMPLATE}.${SUFFIX_A_MIX}.${SUFFIX_B_MIX}"
        RM_MIX_FILE="${RM_TEMPLATE}.${SUFFIX_A_MIX}.${SUFFIX_B_MIX}"
        if [ "${ROT_PRM}" != "" ]; then #if global rot_prm file is given (i.e. PIPER-style)
            RM_MIX_FILE=${ROT_PRM}
        fi

  	#construct pairwise rmsd matrix
        "${DIR}"/utils_libmol/pwrmsd ${REC_PDB} ${LIG_PDB} "${DIR}/prms/atoms.0.0.4.prm.ms.3cap+0.5ace.Hr0rec"  ${RM_MIX_FILE} "./" ${SUFFIX_A_MIX} ${NRES_OTHERS} 1 1 ${SUFFIX_B_MIX}
	#cluster
        "${DIR}"/utils_libmol/cluster -f "clustermat.${SUFFIX_A_MIX}.${SUFFIX_B_MIX}.1" -d "new" -n ${NRES_OTHERS} -u ${NRES_OTHERS} -l 50 -s 10  -p 1 -r 9

	#build models
	"${DIR}"/gen_pdb_cluster_models.0.0.4.pl "clustermat.${SUFFIX_A_MIX}.${SUFFIX_B_MIX}.1.cluster"  ${FT_MIX_FILE}  ${RM_MIX_FILE}  ${LIG_PDB}  "lig.${SUFFIX_A_MIX}"  ${MAX_MODELS}

	#get top model under 10 A rmsd
        if [ "${REF_LIG_PDB}" != "" ]; then
            RMSD_MIX_FILE="${RMSD_TEMPLATE}.${SUFFIX_A_MIX}.${SUFFIX_B_MIX}"
            NAT_CLUSTER_MIX_FILE="${NAT_CLUSTER_TEMPLATE}.${SUFFIX_A_MIX}.${SUFFIX_B_MIX}.txt"
            "${DIR}"/utils_libmol/cluster_number "clustermat.${SUFFIX_A_MIX}.${SUFFIX_B_MIX}.1.cluster" ${RMSD_MIX_FILE} ${NAT_CLUSTER_MIX_FILE}
	fi
    fi
fi

