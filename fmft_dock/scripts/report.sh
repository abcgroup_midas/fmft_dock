#!/bin/bash
#set -euo pipefail
# This script prepares a report for a series of targets.

# Usage: ./report.sh weights.txt [optional arguments.]

if [ "$#" -lt 1 ]; then
	echo "Illegal number of arguments"
	echo "usage:"
	echo "./report.sh weights.txt [optional arguments.]"
	exit 1
fi

#get required arguments
WEIGHTS=${1}	#weights file
shift
DIR_W="$( cd "$( dirname "${WEIGHTS}" )" && pwd )"

#parce optional arguments
CLUSTER=0
OTHERS=0
while [[ $# > 0 ]]
do
    key="$1"
    case $key in
        -o|--others)
            OTHERS=1
            shift   # past argument
        ;;
        -h|--help)
            echo -e "help requested"; exit
            shift   # past argument
        ;;
        *)
            echo "unknown option: ${1}"; exit  # unknown option
        ;;
    esac
done

#read weights from file (in fact only the label - the first number in each line - is used)
WARR=();
index=0
while read line; do
    if [ ! -z "$line" ]; then
        WARR[${index}]="${line}"
	index=$(($index+1))
    fi
done < ${WEIGHTS}

TARGETS=$(ls -d */ | sed 's#\/##g')

#iterate weight sets
RMSD_TEMPLATE="rmsd"
NAT_CLUSTER_TEMPLATE="native_cluster"
REPORT_TEMPLATE="report"
for ((i=0; i < ${#WARR[@]}; i++));
do
    WEIGHT_ID=$(echo ${WARR[${i}]} | awk '{print $1}')
    SUFFIX_A=$(printf "%03d" ${WEIGHT_ID})
    SUFFIX_B="00"

    RMSD_FILE="${RMSD_TEMPLATE}.${SUFFIX_A}.${SUFFIX_B}"
    NAT_CLUSTER_FILE="${NAT_CLUSTER_TEMPLATE}.${SUFFIX_A}.${SUFFIX_B}.txt"
    REPORT_FILE="${REPORT_TEMPLATE}.${SUFFIX_A}.${SUFFIX_B}"
    rm -f ${REPORT_FILE}
    for TARGET in ${TARGETS};
    do
	if [ -f  "${TARGET}/${RMSD_FILE}" ]; then
            HITS=$(awk -v count=0 '$2 < 10 {count +=1} END {print count}' ${TARGET}/${RMSD_FILE})
        else
            HITS="-"
        fi

	if [ -f  "${TARGET}/${NAT_CLUSTER_FILE}" ]; then
            NAT_CLUSTER=$(cat ${TARGET}/${NAT_CLUSTER_FILE} | head -n 1)
        else
            NAT_CLUSTER="- - -"
        fi
	echo -e "${TARGET}\t${HITS}\t${NAT_CLUSTER}" >> ${REPORT_FILE}
    done
done

if [ ${OTHERS} != 0 ]; then
    WEIGHT_ID=${#WARR[@]}
    SUFFIX_A_MIX=$(printf "%03d" ${WEIGHT_ID})
    SUFFIX_B_MIX="00"

    RMSD_MIX_FILE="${RMSD_TEMPLATE}.${SUFFIX_A_MIX}.${SUFFIX_B_MIX}"
    NAT_CLUSTER_MIX_FILE="${NAT_CLUSTER_TEMPLATE}.${SUFFIX_A_MIX}.${SUFFIX_B_MIX}.txt"
    REPORT_MIX_FILE="${REPORT_TEMPLATE}.${SUFFIX_A_MIX}.${SUFFIX_B_MIX}"
    rm -f ${REPORT_MIX_FILE}
    for TARGET in ${TARGETS};
    do
	if [ -f  "${TARGET}/${RMSD_MIX_FILE}" ]; then
            HITS=$(awk -v count=0 '$2 < 10 {count +=1} END {print count}' ${TARGET}/${RMSD_MIX_FILE})
        else
            HITS="-"
        fi

	if [ -f  "${TARGET}/${NAT_CLUSTER_MIX_FILE}" ]; then
            NAT_CLUSTER=$(cat ${TARGET}/${NAT_CLUSTER_MIX_FILE} | head -n 1)
        else
            NAT_CLUSTER="- - -"
        fi
	echo -e "${TARGET}\t${HITS}\t${NAT_CLUSTER}" >> ${REPORT_MIX_FILE}
    done
fi
