#!/bin/bash
#set -euo pipefail
# This script executes the docking routine for a series of receptor-ligand pairs located in subdirectories of the current directory.
# Docking is executed with default parameters.

# Usage: ./dock_series.sh weights.txt [optional arguments]

if [ "$#" -lt 1 ]; then
	echo "Illegal number of arguments"
	echo "usage:"
	echo "./dock_series.sh weights.txt [optional arguments]"

        echo "optional arguments:"
	echo "-p   |--proc_count     (ex: 4)                Number of CPU cores to use (default: 1)"
	echo "-c   |--cluster                               Perform clustering of the results (default: off)"
	echo "-r   |--ref_lig_given  			    Will treat ligand pdb as reference ligand pdb"
	echo "-o   |--others                                Others mode (default: off)"

	exit 1
fi

#path to scripts & binaries (all supposed to be found in the same dir)
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

#get required arguments
WEIGHTS=${1}	#weights file
shift
WEIGHTS_ABS="$( cd "$( dirname "${WEIGHTS}" )" && pwd )/$( basename "${WEIGHTS}" )"
echo ${WEIGHTS_ABS}

#parce optional arguments
PROC_COUNT=1
CLUSTER=0
REF_LIG_GIVEN=0
OTHERS=0
while [[ $# > 0 ]]
do
    key="$1"
    case $key in
        -p|--proc_count)
            PROC_COUNT=${2}
            shift 2 # past argument
        ;;
        -c|--cluster)
            CLUSTER=1
            shift   # past argument
        ;;
        -r|--ref_lig_given)
            REF_LIG_GIVEN=1
            shift   # past argument
        ;;
        -o|--others)
            OTHERS=1
            shift   # past argument
        ;;
        *)
            echo "unknown option: ${1}"; exit  # unknown option
        ;;
    esac
done

OPT_PRMS_STR="-p ${PROC_COUNT}"
if [ ${CLUSTER} != 0 ]; then
    OPT_PRMS_STR="${OPT_PRMS_STR} -c"
fi
if [ ${OTHERS} != 0 ]; then
    OPT_PRMS_STR="${OPT_PRMS_STR} -o"
fi

TARGETS=$(ls -d */ | sed 's#\/##g')
for TARGET in ${TARGETS};
do
    cd ${TARGET}
    REC_PDB="${TARGET}_r_nmin.pdb"
    LIG_PDB="${TARGET}_l_nmin.pdb"
    if [ ${REF_LIG_GIVEN} != 0 ]; then
	REF_LIG_PDB=${LIG_PDB}
        OPT_PRMS_STR="${OPT_PRMS_STR} -r ${REF_LIG_PDB}"
    fi
    bash "${DIR}"/dock_pair.sh ${REC_PDB} ${LIG_PDB} "${WEIGHTS_ABS}" ${OPT_PRMS_STR}
    cd ..
done
