#! /usr/bin/perl -w

##########################################################
# gen_moved_pdb.pl
# -------------------
# copyright : (C) 2006 by Ryan Brenke
# email : rbrenke@gmail.com
##########################################################

use strict;
use File::Basename;

if (scalar @ARGV < 5)
{
	print "usage: $0 ROTPRM FTDATFILE FTDATFILE_I PDB_IFILE PDB_OFILE\n";
	exit;
}

my $dir = dirname(__FILE__);
# command line args
my $rotprm = shift @ARGV;
my $datfile = shift @ARGV;
my $datfile_i = shift @ARGV; # datfile index (0 based)
my $ligand = shift @ARGV;
my $ofile = shift @ARGV; # moved pdb ofile (- is STDOUT)

if ($ligand eq $ofile)
{
	print "PDB_IFILE is identical to PDB_OFILE, exiting\n";
	exit;
}

# read in ftdatfile
open (DATFILE, "< $datfile") or die "could not open $datfile: $!\n";
my @datlines = <DATFILE>;
close DATFILE;
if (scalar @datlines < 1)
{
	die "error: $datfile contains no data\n";
}
if (scalar @datlines <= $datfile_i)
{
	exit; # $datlines[datfile_i] doesn't exist
}
my $datline = $datlines[$datfile_i];
$datline =~ s/^\s+//; # remove leading whitespace
my @words = split /\s+/, $datline;
# get relevant values
my $roti = $words[0]; # rotation index into rotprm
my $tx = $words[1]; # x translation
my $ty = $words[2]; # y translation
my $tz = $words[3]; # z translation
#print "$tx, $ty, $tz\n";


# read in rotprm
open (ROTFILE, "< $rotprm") or die "could not open $rotprm: $!\n";
my @rotlines = <ROTFILE>;
close ROTFILE;
if (scalar @rotlines < 1)
{
	die "error: $rotprm contains no data\n";
}
my $rotline = $rotlines[$roti]; # get rotation roti
#print "$rotline\n";
$rotline =~ s/^\s+//; # remove leading whitespace
# get values
my $rotseti;
my $a11; my $a12; my $a13;
my $a21; my $a22; my $a23;
my $a31; my $a32; my $a33;
($rotseti, $a11,$a12,$a13,$a21,$a22,$a23,$a31,$a32,$a33) = split(/\s+/, $rotline);
#print "$a11 $a12 $a13 $a21 $a22 $a23 $a31 $a32 $a33\n";

my $tmppdb="$$.pdb";
my $vs_line = `$dir/center.pl $ligand $tmppdb`;
my @vs=split /\s+/, $vs_line;
# read in ligand pdb file
open (LIG, "< $tmppdb") or die "couldn't open $ligand: $!\n";
my @liglines = <LIG>;
close LIG;
chomp @liglines; # remove newlines
system "rm $tmppdb";

# print the ofile
open (OFILE, "> $ofile") or die "could not open $ofile: $!\n";
printf OFILE ("MODEL\n");
for my $line (@liglines)
{
	next if ($line !~ /^ATOM/);

	my $prefix = substr ($line, 0, 30);
	my $suffix = substr ($line, 54);

	my $x = substr ($line, 30, 8);
	my $y = substr ($line, 38, 8);
	my $z = substr ($line, 46, 8);
#print "$x, $y, $z\n";

	# rotate and translate
	my $newx = $a11*$x + $a12*$y + $a13*$z + $tx + $vs[0];
	my $newy = $a21*$x + $a22*$y + $a23*$z + $ty + $vs[1];
	my $newz = $a31*$x + $a32*$y + $a33*$z + $tz + $vs[2];

	printf OFILE ("%30s%8.3f%8.3f%8.3f%-50s\n", $prefix, $newx, $newy, $newz, $suffix);
}
printf OFILE ("ENDMDL\n");
close OFILE;
