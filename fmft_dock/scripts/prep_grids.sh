#!/bin/bash
set -euo pipefail
# Prepares grids using PIPER.

# Usage: ./prep_grids.sh grids.txt rec.pdb lig.pdb

if [ "$#" -ne 3 ]; then
	echo "Illegal number of arguments"
	exit 1	
else
	echo -e "\033[36mRunning prep_grids.sh (${1}) (${2}) (${3})\033[0m"
fi

#path to scripts & binaries (all supposed to be found in the same dir)
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
GRIDS_FILE=${1}	#name of grids file to be created
REC_PDB=${2}	#receptor pdb file
LIG_PDB=${3}	#ligand pdb file      

#fixed paramenters:
ATOM_PRM="${DIR}/prms/atoms.0.0.4.prm.ms.3cap+0.5ace.Hr0rec"
ATOM_PRM_MOD="${DIR}/prms/atoms.0.0.4_modified.prm.ms.3cap+0.5ace.Hr0rec"
ROT_PRM="${DIR}/prms/rot70k.0.0.4.prm"

#create a dummy coefficient file for piper
echo " "> dummy.txt

#check that all files exist
if [ ! -f ${REC_PDB} ]; then
	echo "${REC_PDB} not found, aborting."
	exit 1
elif [ ! -f ${LIG_PDB} ]; then
	echo "${LIG_PDB} not found, aborting."
	exit 1
elif [ ! -f ${ATOM_PRM} ]; then
	echo "${ATOM_PRM} not found, aborting."
	exit 1
elif [ ! -f ${ROT_PRM} ]; then
	echo "${ROT_PRM} not found, aborting."
	exit 1
fi

#run grid_gen to generate grids
"${DIR}"/grid_gen.x86_64 -p ${ATOM_PRM}     -f ./dummy.txt -r ${ROT_PRM} -c1.0 -k4 -R 1 ${REC_PDB} ${LIG_PDB}
#${DIR}/grid_gen.x86_64 -p ${ATOM_PRM}     -f ./dummy.txt -r ${ROT_PRM} -c1.0 -k4 -R 1 ${REC_PDB} ${LIG_PDB} --maskrec=${REC_MASK_PDB} --masklig=${LIG_MASK_PDB} --maskr=0.5 
#${DIR}/repulsion      -p ${ATOM_PRM_MOD} -f ./dummy.txt -r ${ROT_PRM} -c1.0 -k4 -R 1 ${REC_PDB} ${LIG_PDB}

echo "8" > ${GRIDS_FILE}
echo "rec_rvdw.dx  lig_rvdw.dx" >> ${GRIDS_FILE}
echo "rec_avdw.dx  lig_avdw.dx" >> ${GRIDS_FILE}
echo "rec_coul.dx  lig_coul.dx" >> ${GRIDS_FILE}
echo "rec_born.dx  lig_coul.dx" >> ${GRIDS_FILE}
echo "rec_eigen_0.dx  lig_eigen_0.dx" >> ${GRIDS_FILE}
echo "rec_eigen_1.dx  lig_eigen_1.dx" >> ${GRIDS_FILE}
echo "rec_eigen_2.dx  lig_eigen_2.dx" >> ${GRIDS_FILE}
echo "rec_eigen_3.dx  lig_eigen_3.dx" >> ${GRIDS_FILE}
#echo "rec_beta_rep_A.dx  lig_rvdw.dx"       >> ${GRIDS_FILE}
#echo "rec_rvdw.dx        lig_beta_rep_B.dx" >> ${GRIDS_FILE}

rm -f dummy.txt
exit 0
