#!/bin/bash

#preprocess pdb files
x="1udi_r_nmin"
../../install-local/pdb_prep/prepare.py $x.pdb
cp ${x}_pmin.pdb rec.pdb
cp ${x}_pmin.psf rec.psf

x="1udi_l_nmin"
../../install-local/pdb_prep/prepare.py $x.pdb
cp ${x}_pmin.pdb lig.pdb
cp ${x}_pmin.psf lig.psf

# run docking
bash ../../install-local/dock_pair.sh    rec.pdb lig.pdb ../../install-local/prms/mfft_weights_ei.txt --proc_count 1


# cluster results and build docking models
bash ../../install-local/cluster_pair.sh rec.pdb lig.pdb ../../install-local/prms/mfft_weights_ei.txt --ref_lig lig.pdb
