#!/bin/bash
# ./gen_trans_matrices.sh Z_START Z_STOP Z_STEP
# Generates translation matrices for all translations from Z_START to Z_STOP Angstrom
# with a step of Z_STEP Angstrom and writes them to ./build/data/.
#
Z_START=${1}
Z_STOP=${2}
Z_STEP=${3}
mkdir -p install-local/data
echo -e "Genertaing translation matrix files for all translations in range ${Z_START}...${Z_STOP} with a step of ${Z_STEP}"
echo -e "\033[33m Running command: ./install-local/utils/tab_trans_matrix ${Z_START} ${Z_STOP} ${Z_STEP} \033[0m"
./install-local/utils/tab_trans_matrix ${Z_START} ${Z_STOP} ${Z_STEP}
